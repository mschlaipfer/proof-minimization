(********************************************************)
(** Module for reading resolution chain proofs.
    This format is used by MiniSAT to represent proofs.
    @author Georg Weissenbacher                         *)

open Util

(********************************************************)
(** Literals -- same as in ResolutionProof              *)

module Literal = ResolutionProof.Literal

(********************************************************)
(** Proof nodes                                         *)

type node = Root of Literal.t list 
            | Chain of Graph.vertex list

module Node : Proof.FORMULA with type t = node =
struct
  type t = node
end

(********************************************************)
(** Resolution chain proof                              *)
include Proof.Make (Node)

(********************************************************)
(* Convert resolution chains to resolution proof        *)

(** Convert a chain into a sequence of resolution steps *)
let rec convert_chain proof chain prev next final =
  match chain with
    [] -> raise (Invalid_proof "Chain of length 0")
  | [r] -> ResolutionProof.add_step proof [ prev; r ] final;
    let _ = ResolutionProof.add_vertex ~id:prev proof in
    let _ = ResolutionProof.add_vertex ~id:r proof in
    assert (List.length (ResolutionProof.get_premises proof final) = 2);
    next
  | r::rs -> ResolutionProof.add_step proof [ prev; r ] next; 
    let _ = ResolutionProof.add_vertex ~id:prev proof in
    let _ = ResolutionProof.add_vertex ~id:r proof in
    assert (List.length (ResolutionProof.get_premises proof next) = 2);
    convert_chain proof rs next (next+1) final

(** Convert a node of a chain proof with slicing        *)
let rec convert_node rcp proof visited node next =
  if Hashtbl.mem visited node then next
  else begin
    Hashtbl.add visited node true;
    let _ = ResolutionProof.add_vertex ~id:node proof in
    let label = get_vertex_label rcp node in
    match label with
      Root cl ->                         (* reuse node id *)
        ResolutionProof.add_vertex_label proof node (cl, None); next
    | Chain ch -> 
			let n = 
        convert_chain proof (List.tl ch) (List.hd ch) next node
      in List.fold_right (convert_node rcp proof visited) ch n
  end

(** Convert a node of a chain proof without slicing     *)
let rec to_unsliced_resolution_proof rcp =
  match rcp.sink with
    None -> raise (Invalid_proof "Proof has no sink")
  | Some sink ->
    let proof = ResolutionProof.create () in
    let process node label next = 
      let _ = ResolutionProof.add_vertex ~id:node proof in
      match label with
        Root cl ->                         (* reuse node id *)
          ResolutionProof.add_vertex_label proof node (cl, None); next
      | Chain ch -> 
        convert_chain proof (List.tl ch) (List.hd ch) next node
    in
    let _ = Hashtbl.fold process rcp.ell (sink+1) in
    ResolutionProof.set_sink proof sink;
    proof

(** Convert a proof starting with sink                  *)
let convert_proof rcp proof =
  match rcp.sink with
    None -> raise (Invalid_proof "Proof has no sink")
  | Some sink ->
    let visited = Hashtbl.create 100 in
    let _ = convert_node rcp proof visited sink (sink+1) in
    ResolutionProof.set_sink proof sink

(** Convert proof to resolution proof                   *)
let to_resolution_proof rc_proof =
  let result = ResolutionProof.create () in
  convert_proof rc_proof result;
  result

(********************************************************)
(* Export resolution chain proofs to files              *)

(** write a resolution chain proof to a channel         *)
let to_channel chan proof = 
  let print_vertex id = 
    match get_vertex_label proof id with
      Root clause -> 
        output_string chan (string_of_int id); 
        output_char chan ' ';
	      ResolutionProof.output_clause chan clause;
		    output_string chan " 0 0\n";
    | Chain preds ->
      output_string chan (string_of_int id); 
      output_string chan " * ";
      List.iter (function pid -> 
	      output_string chan (string_of_int pid); 
        output_char chan ' ') preds;
      output_string chan "0\n";
  in 
  visit proof print_vertex print_vertex
