/**
   Wrapper code for calling MiniSAT from OCaml
 */

extern "C" {

#include <caml/memory.h>
#include <caml/alloc.h>
#include <caml/fail.h>

}

#include <zlib.h>

#include <core/Solver.h>
#include <core/Dimacs.h>

extern "C" {
  CAMLprim value solve (value ml_str)
  {
    /* convert Ocaml string into native string */
    CAMLparam1 (ml_str);
    CAMLlocal2 (ml_clauses, ml_learnt);
    CAMLlocal1 (ml_result);

    char *name = String_val (ml_str);
    gzFile in = gzopen (name, "rb");
    if (in == NULL)
      caml_failwith ("Failed to open file");

    Minisat::Solver solver;

    try 
    {
      parse_DIMACS (in, solver);
      gzclose (in);
    } 
    catch (...) 
    {
      caml_failwith ("DIMACS parsing exception");
    }

    bool sat;
    try {
      sat = solver.solve();
    }
    catch (...)
    {
      caml_failwith ("Solver exception");
    }

    ml_result = caml_alloc (3, 0);
    ml_clauses = solver.clog.clauses ();
    ml_learnt = solver.clog.learnt ();
    
    Store_field (ml_result, 0, Val_bool (sat));
    Store_field (ml_result, 1, ml_clauses);
    Store_field (ml_result, 2, ml_learnt);
    CAMLreturn (ml_result);
  }
}
