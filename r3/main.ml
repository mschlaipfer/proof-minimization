open Printf

let main () = 
  if (Array.length Sys.argv < 2) then
    begin
      print_string "Expected one parameter\n";
      exit 0;
    end;
  let file_name = Sys.argv.(1) in
  let proof = 
    try
      ResolutionProof.from_file file_name 
    with ResolutionProof.Syntax_error _ ->
      let pr = Sat.from_dimacs file_name in
      ResolutionChainProof.to_resolution_proof pr
  in
  ResolutionProof.to_channel stdout proof;
  assert (ResolutionProof.check_proof proof);;
  (* let pitp = 
    Interpolation.interpolate_pudlak proof 
      (fun v -> if v < 5 then Interpolation.APartition 
        else Interpolation.BPartition)
  in Interpolation.output_interpolant stdout pitp ;;  *)

main ();
