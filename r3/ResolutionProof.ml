(********************************************************)
(** Module for reading ResolutionProofs from files      
    @author Georg Weissenbacher                         *)

open Util

(********************************************************)
(** Propositional literals                              *)
module Literal :
sig
  type t

  (** @return true if the literal is signed *)
  val signed : t -> bool

  (** @return variable id of the literal *)
  val variable :  t -> int

  (** @return the sign of a literal *)
  val make : int -> bool -> t

  (** @return the negation of the argument *)
  val negate : t -> t

  (** compares two literals *)
  val compare : t -> t -> int
end =
struct
  type t = int
  let signed lit = (lit land 1) = 1
  let variable lit = lit lsr 1
  let make var sign = (var + var) lxor ((function true -> 1 | _ -> 0) sign)
  let negate lit = (lit lxor 1)
  let compare l0 l1 = l0 - l1
end

(********************************************************)
(** Propositional clauses with optional pivot literal   *)

module ClauseWithPivot : Proof.FORMULA 
  with type t = Literal.t list * Literal.t option =
struct
  type t = Literal.t list * Literal.t option
end

(********************************************************)
(** Propositional resolution proof                      *)
include Proof.Make (ClauseWithPivot) 

exception Syntax_error of string

(********************************************************)
(** internal proof structure for ABC proofs             *)
type node = Root of Graph.vertex list 
	          | Step of Graph.vertex list 

(********************************************************)
(** Parse a node following a clause id                  *)
let parse_node tokens = 
  let process_token lits t = 
    match t with
      Genlex.Int n -> lits := n :: (!lits) 
    | _ -> raise (Syntax_error "Expected literal")
  in
  
  (* function to parse a chain *)
  let parse_step () =
    let verts : Graph.vertex list ref = ref [] in
    Stream.iter (process_token verts) tokens; 
    (* sanity check: first element is 0 *)
    if (List.hd !verts <> 0) then
      raise (Syntax_error "Expected 0 terminated line");
    Step (List.tl !verts)
  in

  (* function to parse a root clause *)
  let parse_root n =
    let ids : int list ref = ref [ n ] in
    Stream.iter (process_token ids) tokens;
    (* sanity check: first two elements are 0 *)
    if List.hd !ids <> 0 or ((List.hd (List.tl !ids)) <> 0) then
      raise (Syntax_error "Expected 0 0 terminated line");
    Root (List.rev (List.tl (List.tl !ids)))

  (* use first token to identify roots and chains *)
  in match (Stream.next tokens) with
    Genlex.Kwd _ -> parse_step ()
  | Genlex.Int n -> parse_root n
  | _ -> raise (Syntax_error "Expected literal or *")

(********************************************************)
(** read a line from the file and tokenize it           *)
let read_line cin =
  try
    (* generate a lexer for clauses *)
    let line = input_line cin in
    let lexer = Genlex.make_lexer [ "*" ] in
    let tokens = lexer (Stream.of_string line) in
    
    (* read the first integer representing the clause id *)
    let clause_id =
      match (Stream.next tokens) with
	      Genlex.Int n -> n
      | _ -> raise (Syntax_error "Expected clause id")
    in (clause_id, parse_node tokens)
  with Parsing.Parse_error -> raise (Syntax_error "Parsing error")
    
(********************************************************)
(** add a root or chain node to a resolution proof      *)
let add_node proof (id, n) =
  match n with
    Root c ->  
      let lits = List.map 
	      (function v -> Literal.make (abs v) (v<0)) c
      in
      let _ = add_vertex ~id:id proof in
      add_vertex_label proof id (lits, None)

  | Step c -> add_step proof c id


(********************************************************)
(** read a proof in ABC format from a channel           *)
let from_channel pin =
  let proof = create () in
  begin
    try
      while true do
	      let (id, node) = read_line pin in
	      add_node proof (id, node);
	      set_sink proof id; (* remember most recent sink *)
      done
    with End_of_file -> ()
  end;
  proof

(** pretty print a literal
    @param chan the output channel
    @param l the literal to print
*)
let output_literal chan (l: Literal.t) = 
	if (Literal.signed l) then output_char chan '-';
	output_string chan (string_of_int (Literal.variable l + 1))
    
(** pretty print a clause
    @param chan the output channel
    @param clause a clause of type Clause.t
*)
let output_clause chan clause =
  match clause with
    [] -> ()
  | _ -> output_literal chan (List.hd clause);
    List.iter
	    (fun lit -> output_char chan ' '; output_literal chan lit;) 
	    (List.tl clause)

(********************************************************)
(** write a resolution proof in ABC format to a channel *)
let to_channel chan proof = 
  let print_leaf id = 
    let (clause, _) = get_vertex_label proof id in
    output_string chan (string_of_int id);
    output_char chan ' ';
    output_clause chan clause;
    output_string chan " 0 0\n";
  in
  let print_inner id =
    let preds = Graph.predecessors proof.dag id in
    output_string chan (string_of_int id); 
    output_string chan " * ";
    List.iter (function pid ->  
 	    output_string chan (string_of_int pid); 
      output_char chan ' ') preds;
    output_string chan "0\n";
  in
  visit proof print_leaf print_inner

(********************************************************)
(** write a resolution proof in ABC format w/o slicing  *)
let to_channel_unsliced chan proof = 
  let print id =
    let predecessors = 
      try 
        Graph.predecessors proof.dag id
      with Not_found -> []
    in
    match predecessors with
      [] -> begin
        let (clause, _) = get_vertex_label proof id in
        output_string chan (string_of_int id);
        output_char chan ' ';
        output_clause chan clause;
        output_string chan " 0 0\n";
      end
    | preds -> begin
      output_string chan (string_of_int id); 
      output_string chan " * ";
      List.iter (function pid ->  
 	      output_string chan (string_of_int pid); 
        output_char chan ' ') preds;
      output_string chan "0\n";
    end
  in 
  iter print proof;
  match proof.sink with 
    None -> ()
  | Some sink -> begin
    output_string chan "sink: ";
    output_string chan (string_of_int sink);
    output_char chan '\n'
  end

(********************************************************)
(** read a resolution proof in ABC format from a file   *)
let from_file file_name = 
  let ic = open_in file_name in
  try
    from_channel ic
  with e ->
    close_in_noerr ic;
    raise e

(********************************************************)
(** write a resolution proof in ABC format to a file    *)
let to_file file_name proof = 
  let ic = open_out file_name in
  try
    to_channel ic proof
  with e ->
    close_out_noerr ic;
    raise e
    
      
(********************************************************)
(** pretty print dotty graph to stdout                  *)
let pretty_print proof =
  let rec print_node id = 
    match (Graph.predecessors proof.dag id) with
      [] -> let (clause, _) = get_vertex_label proof id in
	          begin
	            print_char 'n'; print_int id; 
	            print_string " [label=\"";
	            output_clause stdout clause;
	            print_endline "\"];"
	          end
    | preds -> 
      List.iter 
	      (function pid -> 
	        print_char 'n'; print_int pid; 
	        print_string " -> "; 
	        print_char 'n'; print_int id;
	        print_endline ";";
	        print_node pid) 
	      preds;
  in 
  match proof.sink with
    None -> ()
  | Some v -> 
    print_endline "digraph G {";
    print_node v; print_endline "}"

(********************************************************)
(** compute the resolvent of two (sorted) clauses       *)
let sorted_resolve c d = 
  let sorted = List.merge Literal.compare c d in
  let piv = ref None in
  let rec filter_literals clause =
    match clause with 
      [] | [_]-> clause
    | l::m::rest ->
      if (Literal.negate m = l) (* pivot literal *)
      then begin
        match !piv with 
          None ->  piv := Some l; filter_literals rest
        | Some x -> raise (Invalid_proof "More than one pivot")
      end
      else if (m = l) (* merge literal *)
      then filter_literals (m::rest)
      else l::(filter_literals (m::rest))
  in 
  let res = filter_literals sorted in
  match !piv with
    None -> raise (Invalid_proof "No pivot literal")
  | Some l -> assert (not (Literal.signed l)); (l, res)

(** recursively compute the resolvent of two clauses    *)
let rec resolvents proof cache vertex =
  let preds = Graph.predecessors proof.dag vertex in
  match preds with 
    [] -> 
      (try 
         Hashtbl.find cache vertex (* already available? *)
       with Not_found ->
         let clause =  
           List.sort Literal.compare (fst (get_vertex_label proof vertex))
         in
         Hashtbl.add cache vertex clause; clause)
        
  | _ -> (assert ((List.length preds) = 2);
          try 
            fst (get_vertex_label proof vertex) (* already available? *)
          with Not_found ->
            let cs = List.map (resolvents proof cache) preds in
            let (piv, res) = 
              sorted_resolve (List.hd cs) (List.hd (List.tl cs)) in
            add_vertex_label proof vertex (res, Some piv);
            res)

(** add resolvents as labels of a proof                 *)
let check_proof proof =
  match proof.sink with
    None -> false
  | Some root -> 
    try
      let root_cache = Hashtbl.create 100 in
      let res = resolvents proof root_cache root in
      ((List.length res) = 0)
    with Invalid_proof msg -> prerr_string (msg ^"\n"); false

(** compute the resolvent of two clauses         
    @return the pivot literal and a resolvent
*)
let resolve c d = 
  sorted_resolve
    (List.sort Literal.compare c) (List.sort Literal.compare d)
