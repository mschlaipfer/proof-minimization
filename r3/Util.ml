(********************************************************)
(** Module with utility implementations (e.g. graphs)   *)
(** @author Georg Weissenbacher                         *)

(********************************************************)
(** A directed graph structure                          *)
module type GRAPH = 
sig
  type vertex = int
  type t

  (** generates an emtpy graph *)
  val create : unit -> t

  (** adds a vertex to the graph *)
  val add_vertex : ?id:vertex -> t -> vertex

  (** returns the maximal vertex id *)
  val max_vertex : t -> vertex

  (** adds a directed edge to the graph *)
  val add_edge : t -> vertex -> vertex -> unit

  (** removes a directed edge to the graph *)
  val remove_edge : t -> vertex -> vertex -> unit

  (** @return successors of a vertex *)
  val successors : t -> vertex -> vertex list

  (** @return predecessors of a vertex *)
  val predecessors : t -> vertex -> vertex list
end

module Graph : GRAPH =
struct
  type vertex = int
  type t = {
    mutable next : vertex ;
    edges: (vertex, vertex list * vertex list) Hashtbl.t
  }

  (** generates an empty graph *)
  let create () = 
    {
			next = 0;
			edges = Hashtbl.create 100;
    }

  (** adds a vertex to the graph
   *  @return a new id if no id is provided, id otherwise
   *)
  let add_vertex ?id g = 
		match id with
			None -> let v = g.next in	g.next <- v + 1; v
		| Some v when v >= g.next -> g.next <- (v + 1) ; v
		| Some v -> v

  (** returns the maximal vertex id *)
  let max_vertex g = (g.next - 1)

	(** adds a directed edge to the graph *)
	let add_edge g (u:vertex) (v:vertex) = 
		let (upred, usucc) = 
			try 
				Hashtbl.find g.edges u 
			with Not_found -> ([], [])
		in 
		let (vpred, vsucc) = 
			try 
				Hashtbl.find g.edges v 
			with Not_found -> ([], [])
		in begin
			let insert e es = if List.mem e es then es else (e :: es) in
			Hashtbl.replace g.edges u (upred, insert v usucc);
			Hashtbl.replace g.edges v (insert u vpred, vsucc)
		end

					(** removes a directed edge from the graph *)
	let remove_edge g (u:vertex) (v:vertex) = 
		try
			let (upred, usucc) = Hashtbl.find g.edges u in
			let (vpred, vsucc) = Hashtbl.find g.edges v in
			let remove_vertex n = List.filter (function i -> i <> n) in
			begin
				Hashtbl.replace g.edges u (upred, remove_vertex v usucc);
				Hashtbl.replace g.edges v (remove_vertex u vpred, vsucc)
			end
		with Not_found -> ()

	(** @return successors of a vertex *)
	let successors g (v:vertex) = 
		let (_, succ) = Hashtbl.find g.edges v in succ

	(** @return predecessors of a vertex *)
	let predecessors g (v:vertex) = 
		let (pred, _) = Hashtbl.find g.edges v in pred
end
