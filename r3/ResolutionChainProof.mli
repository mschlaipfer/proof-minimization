(********************************************************)
(** ResolutionChainProof Interfaces                          
    @author Georg Weissenbacher                         *)

open Util

type node = Root of ResolutionProof.Literal.t list 
            | Chain of Graph.vertex list

module Node : Proof.FORMULA with type t = node

include Proof.S with type formula = Node.t

(** write a resolution chain proof to a channel         *)
val to_channel : out_channel -> t -> unit

(** convert resolution chain proof to resolution proof  *)
val to_resolution_proof : t -> ResolutionProof.t

(** convert resolution chain proof, no slicing          *)
val to_unsliced_resolution_proof : t -> ResolutionProof.t
