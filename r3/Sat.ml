(********************************************************)
(** Interfacing with MiniSAT                            
    @author Georg Weissenbacher                         *)

module Lit = ResolutionProof.Literal

exception Sat_error of string
exception Root_conflict of int

let empty = (Array.make 0 (Lit.make 0 false))

type clause = Lit.t array

external solve : string -> 
  bool * clause list * clause list = "solve" 

(* data structure for reconstructing a resolution proof *)
type t = {
  clauses      : (int, clause) Hashtbl.t ;
  mutable next : int ;           (* next free clause id *)
  watches      : (Lit.t, int list) Hashtbl.t ;
  assignments  : (Lit.t, int) Hashtbl.t ;
  trail        : (Lit.t * int) Stack.t ;
  queue        : (Lit.t * int) Queue.t ;
  proof        : ResolutionChainProof.t ;
}

(** create a structure for reconstructing a proof       *)
let create () =
  { 
    clauses = Hashtbl.create 100;
    watches = Hashtbl.create 100;
    assignments = Hashtbl.create 100;
    trail = Stack.create();
    queue = Queue.create();
    proof = ResolutionChainProof.create ();
    next = 0;
  }

(********************************************************)
(* Proof modification functions                         *)

(** add root node to the proof                          *)
let add_root solver cid c =
  let n = ResolutionChainProof.add_vertex 
    ~id:cid solver.proof 
  in
  ResolutionChainProof.add_vertex_label 
    solver.proof n 
    (ResolutionChainProof.Root (Array.to_list c))

(** add chain node to the proof                         *)
let add_chain solver cid ch =
  let n = ResolutionChainProof.add_vertex 
    ~id:cid solver.proof 
  in 
  ResolutionChainProof.add_step solver.proof ch n;
  ResolutionChainProof.add_vertex_label 
    solver.proof n 
    (ResolutionChainProof.Chain ch)

(********************************************************)
(* Formatting functions                                 *)
(** convert a chain to a string for pretty printing     *)
let chain_to_string chain = 
  let nodes = List.fold_left
    (fun ss l -> (string_of_int l) :: ss) [] chain
  in
  "[" ^ (String.concat " " (List.rev nodes)) ^ "]"

(** convert a clause to a string for pretty printing    *)
let clause_to_string clause = 
  let literal_to_string (l: Lit.t) = 
	  (if (Lit.signed l) then "-" else "") ^
      (string_of_int (Lit.variable l))
  in
  let lits = Array.fold_left
    (fun ss l -> (literal_to_string l) :: ss) [] clause
  in
  String.concat " " (List.rev lits)

(********************************************************)
(* Boolean constraint propagation functions             *)

(** enqueue a literal to be propagated                  *)
let enqueue solver lit reason =
  Queue.push (lit, reason) solver.queue 

(** is the literal in conflict with current assignment? *)
let conflict solver lit =
  Hashtbl.mem solver.assignments (Lit.negate lit)

(** commit assignment                                   *)
let assign solver lit reason =
  Hashtbl.add solver.assignments lit reason

(** undo assignment                                     *)
let unassign solver lit =
  Hashtbl.remove solver.assignments lit 

(** get list of clauses watching this literal           *)
let watchers solver lit =
  try Hashtbl.find solver.watches lit 
  with Not_found -> []

(** add a clause to the watch list                      *)
let watch solver lit id =
  let watched = watchers solver lit in
  Hashtbl.replace solver.watches lit (id :: watched)

(** remove a clause from the watch list                 *)
let unwatch solver lit id =
  let watched = 
    try Hashtbl.find solver.watches lit 
    with Not_found -> []
  in 
  let removed = 
    List.filter (function n -> n <> id) watched 
  in
  Hashtbl.replace solver.watches lit removed

(** check watches for a clause after assignment        
  @param lit the newly assigned literal 
  @param id the id of the clause being watched
  @return true if a new literal has been enqueued
*)
let process_watch solver lit id =
  let c = Hashtbl.find solver.clauses id in
  (* filter tautologies                                 *)
  if Array.fold_left 
    (fun taut elem ->
        taut || conflict solver (Lit.negate elem)) 
    false c
  then
    false (* ignore tautological clause                 *)
  else
    (* find a literal to replace the current watched on *)
    let rec find_unassigned index =
      if index < Array.length c && conflict solver c.(index)
      then find_unassigned (index+1)
      else index
    in
    (* find out which literal we've been watching       *)
    let neg_lit = Lit.negate lit in 
    let lit_index = if c.(0) = neg_lit then 0 else 1 in
    let new_index = find_unassigned 2 in
    if (new_index = Array.length c) 
    then begin
      (* enqueue the _other_ watched literal              *)
      let oth_lit = c.((lit_index + 1) mod 2)	in
      enqueue solver (oth_lit) id;
      true
    end
    else
      (* update the watched clauses                     *)
      begin
        unwatch solver lit id;
        watch solver (Lit.negate c.(new_index)) id;
        c.(lit_index) <- c.(new_index);
        c.(new_index) <- neg_lit;
        false
      end

(** check a list of watched clauses                    
    @return true if a new literal has been enqueued
*)
let process_watches solver lit watched =
  List.map (process_watch solver lit) watched
       
(** propagate next literal in queue                     
    @return conflict clause id, if a conflict arises
*)
let propagate_one solver =
  let (lit, reason) = Queue.take solver.queue in
  (* check for conflicts *)
  if (conflict solver lit) then 
    Some (reason)             (* report conflict clause *)
  else if (conflict solver (Lit.negate lit)) then 
    None                                   (* redundant *)
  else begin
    Stack.push (lit, reason) solver.trail; 
    assign solver lit reason;
    let watched = watchers solver lit in
    let _ = process_watches solver lit watched in
    None                                 (* no conflict *)
  end

(** propagate the elements in the queue                 
    @return a conflict clause id, if a conflict arises
*)
let rec propagate solver = 
  if (Queue.is_empty solver.queue) then None
  else match propagate_one solver with
    None -> propagate solver
  | Some confl -> Some confl

(** check if a learnt clause is already satisfied
    @return true if one of the literals is already set
*)
let is_satisfied solver learnt = 
  Array.fold_left
    (fun red lit -> 
      red || conflict solver (Lit.negate lit))
    false learnt

(** check if first clause subsumes the second           *)
let is_subsumed first second = 
	let lit_list = Array.to_list second in
	Array.fold_left 
		(fun subsumed lit -> subsumed && (List.mem lit lit_list))
		true first

(** check if two clauses are equal                      *)
let is_equal first second = 
  ((Array.length first = Array.length second) &&
    (is_subsumed first second))

(** Resolution function for debugging                   
    @param curr_clause the most recent resolvent
    @param other_id the id of the clause to resolve with
*)
let resolve solver curr_clause other_id = 
  let other_clause = Hashtbl.find solver.clauses other_id in
  let (_, resolvent) =
    ResolutionProof.resolve 
      (Array.to_list curr_clause) (Array.to_list other_clause)
  in Array.of_list resolvent

(** analyse a conflict clause and derive a resolution
    chain for target clause                                   
    @param conflict id obtained by calling propagate
    @param target the clause id of the proof target
*)
let extract_chain solver confl target = 
  let seen = Hashtbl.create 100 in
  (* mark variables in given clause as 'seen'   *)
  let mark clause_id =
    let clause = Hashtbl.find solver.clauses clause_id in
    Array.iter (function lit -> 
      Hashtbl.replace seen (Lit.variable lit) true) clause
  in
  mark confl;    (* mark all literals in conflict clause *)
  (* now traverse the trail *)
  let chain = ref [ confl ] in
  (* track current resolvent for debugging? *)
  let current = ref (Hashtbl.find solver.clauses confl) in
  let visit (lit, reason) =
    if Hashtbl.mem seen (Lit.variable lit) then
      begin
        if (reason <> target) then (* ignore target lits *)
          begin
            current := (resolve solver !current reason);
            chain := reason :: !chain;
            mark reason;
            Hashtbl.remove seen (Lit.variable lit)
          end
      end;
    (* can we cut the search short? *)
    if Hashtbl.length seen = 0 then raise Stack.Empty
  in 
  let _ = 
    try
      Stack.iter visit solver.trail;
    with Stack.Empty -> () 
  in
  (!current, List.rev !chain)           (* return chain  *)

(** add a clause to the problem instance                *)
let add_clause solver c =
  begin
    let id = solver.next in
    solver.next <- id + 1;
    Hashtbl.add solver.clauses id c;
    if (Array.length c = 1) then
      enqueue solver c.(0) id
    else if (Array.length c > 1) then
      begin
        watch solver (Lit.negate c.(0)) id; 
        watch solver (Lit.negate c.(1)) id;
      end;
  end

(** undo assignments until we reach certain trail length *)
let rec cancel_until solver level =
  if (Stack.length solver.trail > level) then 
    let (lit, _) = Stack.pop solver.trail in
    (unassign solver lit; cancel_until solver level)

(** enqeue the negated literals of a learnt clause      
    @return the undo level before enqueuing the literals
*)
let add_assumptions solver learnt reason = 
  let level = Stack.length solver.trail in
  Array.iter 
    (function lit -> 
      enqueue solver (Lit.negate lit) reason)
    learnt;
  level

(** construct a resolution chain for a learnt clause     
    @return a resolvent which subsumes learnt, and a chain
*)
let prove_learnt solver learnt =
  let id = solver.next in
	let undo_level = add_assumptions solver learnt id in
	match propagate solver with
		Some confl ->
			begin
				let (resolvent, chain) = extract_chain solver confl id in
        assert (is_subsumed resolvent learnt);
				cancel_until solver undo_level; 
				Queue.clear solver.queue;   (* don't propagate  *)
				(resolvent, chain)
			end
	| None -> 
		let errmsg = 
			"Failed to prove [" ^ (clause_to_string learnt) ^"]"
		in raise (Sat_error errmsg)
		
(** extract a resolution proof from a SAT instance by
    reconstructing proof from a clause trail            *)
let from_dimacs fname = 
  let (sat, clauses, learnt) = solve fname in
  if sat then raise (Sat_error "Instance is satisfiable");
  let solver = create () in
  let reversed = List.rev clauses in
  (* add the clauses of the original instance           *)
  List.iter (add_clause solver) reversed;
  (* add the clauses to the proof as root nodes         *)
  let _ = List.fold_left
    (fun id clause -> add_root solver id clause; id + 1) 
    0 reversed
	in
  (* construct resolution chains for learnt clauses     *)
	(try
		List.iter 
			(function c ->
				if (not (is_satisfied solver c)) then begin
					match propagate solver with
						None -> let (res, chain) = prove_learnt solver c in
										if (List.length chain) > 1 then
											begin
												(* add chain node to the proof  *)
												add_chain solver solver.next chain;
												add_clause solver res;
											end
					| Some confl ->        (* root level conflict *)
						let (_, chain) = extract_chain solver confl solver.next in
						add_chain solver solver.next chain;
						raise (Root_conflict solver.next)
				end)
			(List.rev (empty :: learnt));
	with Root_conflict v ->
		(* the last node we added is the sink               *)
		ResolutionChainProof.set_sink solver.proof v);
 	solver.proof
