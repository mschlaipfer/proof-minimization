If editing with Emacs, please use the Tuareg mode.
Here's how to activate it:

(add-to-list 'load-path "/path/to/tuareg-2.0.6")
(setq auto-mode-alist
  (cons '("\\.ml\\w?" . tuareg-mode) auto-mode-alist))
(autoload 'tuareg-mode "tuareg" "Major mode for editing Caml code." t)
(autoload 'camldebug "cameldeb" "Run the Caml debugger." t)

For consistent formatting, please add the following hook:

(defun ocaml-coding-rules ()
  (setq tuareg-in-indent 0)
  (setq tuareg-default-indent 2)
  (setq default-tab-width 2))

(add-hook 'tuareg-mode-hook 'ocaml-coding-rules)
