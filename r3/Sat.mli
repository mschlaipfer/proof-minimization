(********************************************************)
(** Sat solver interfaces                          
    @author Georg Weissenbacher                         *)

exception Sat_error of string

val from_dimacs : string -> ResolutionChainProof.t
