(********************************************************)
(** ResolutionProof Interfaces                          
    @author Georg Weissenbacher                         *)

open Util

(********************************************************)
(** Propositional literals                              *)
module Literal :
sig
  type t

  (** @return true if the literal is signed *)
  val signed : t -> bool

  (** @return variable id of the literal *)
  val variable :  t -> int

  (** @return the sign of a literal *)
  val make : int -> bool -> t

  (** @return the negation of the argument *)
  val negate : t -> t

  (** compares two literals *)
  val compare : t -> t -> int
end

(********************************************************)
(** Propositional clauses                               *)

module ClauseWithPivot : Proof.FORMULA 
  with type t = Literal.t list * Literal.t option

exception Syntax_error of string

include Proof.S with type formula = ClauseWithPivot.t

(** read resolution proof in ABC format from a file     *)
val from_file : string -> t 

(** read resolution proof in ABC format from a channel  *)
val from_channel : in_channel -> t 

(** write a resolution proof in ABC format to a file    *)
val to_file : string -> t -> unit

(** print a clause to a channel                         *)
val output_clause : out_channel -> Literal.t list -> unit

(** write a resolution proof in ABC format to a channel *)
val to_channel : out_channel -> t -> unit

(** write a resolution proof in ABC format, unsliced    *)
val to_channel_unsliced : out_channel -> t -> unit

(** print dotty graph to standard out stream            *)
val pretty_print : t -> unit

(** compute the resolvent and the pivot literal         *)
val resolve : Literal.t list -> Literal.t list -> 
  Literal.t * Literal.t list

(** update the resolvents of a proof                    *)
val check_proof : t -> bool

