(********************************************************)
(** Module for extracting interpolants from proofs
    @author Georg Weissenbacher                         *)

open Util
open ResolutionProof

exception Invalid_labelling of string

type label = ALocal | BLocal | Shared | Transparent

type labels = Literal.t -> label

type partial = Lit of Literal.t 
               | Part of Graph.vertex
               | Conj of partial list
               | Disj of partial list

type interpolant = { pi   : (Graph.vertex, partial) Hashtbl.t;
                     lmap : (Graph.vertex, labels) Hashtbl.t;
                     root : Graph.vertex
                   }

type partition = APartition | BPartition

type labelling = Graph.vertex -> (partition * labels)

(********************************************************)
(* lattice functions for merging colour                 *)

(** merge two labels                                    *)
let merge_labels first second =
  if (first = second) then first 
  else if first = Transparent then second
  else if second = Transparent then first
  else Shared

(** merge two label mappings
    @param first label mapping of first clause
    @param second label mapping of second clause
    @param clause the target resolvent 
*)
let merge_label_maps first second resolvent =
  let lbls = Hashtbl.create (List.length resolvent) in
  let wrap lmap lit = 
    try 
      lmap lit 
    with Not_found -> Transparent 
  in
  List.iter
    (function lit ->
      Hashtbl.add lbls lit 
        (merge_labels ((wrap first) lit) ((wrap second) lit)))
    resolvent;
  (* return a function mapping literals to labels      *)
  (Hashtbl.find lbls)

(** project a clause down to a certain label                
    @param lbl the label to project to
    @param clause the clause to be projected
    @param lit_lbls the labelling for the clause
*)
let dproject lbl clause lit_lbls =
  match lbl with
    Transparent -> []    (* Tansparent not in lit_lbls *)
  | Shared -> clause
  | _ -> List.filter (fun lit -> lbl = lit_lbls lit) clause

(********************************************************)
(* functions for simplifying partial interpolants       *)

(** follow a pi vertex until we reach an actual formula *)
let rec follow itp part =
  match part with
    Part vertex -> follow itp (Hashtbl.find itp.pi vertex)
  | _ -> part

(** does partial interpolant simplify to false?         *)
let is_inconsistent itp term =
  match (follow itp term) with
    Disj terms -> (List.length terms) = 0
  | Conj terms -> 
    let followed = List.map (follow itp) terms in
    List.exists (fun t -> t = Disj []) followed
  | _ -> false

(** does partial interpolant simplify to true?          *)
let is_tautology itp term =
  match (follow itp term) with
    Conj terms -> (List.length terms) = 0
  | Disj terms -> 
    let followed = List.map (follow itp) terms in
    List.exists (fun t -> t = Conj []) followed
  | _ -> false

(** impose an order on partial interpolants:
    Lit < Conj < Disj if is_conjunction
    Lit < Disj < Conj if not is_conjunction
*)
let rec compare is_conjunction itp s t = 
  match (follow itp s, follow itp t) with
    (Lit l, Lit m) -> Literal.compare l m
  | (Lit _, _) -> -1
  | (_, Lit _) -> 1
  | (Conj _, Disj _) -> if is_conjunction then 1 else -1
  | (Disj _, Conj _) -> if is_conjunction then -1 else 1
  | (Conj ss, Conj ts) -> 0           (* top level only *)
  | (Disj ss, Disj ts) -> 0
  | (Part _, _)
  | (_, Part _) -> 
    failwith "Unresolved vertex in partial interpolant"

exception Inconsistent
exception Tautology

(** distribute single literal using DeMorgan's rule     *)
let conjunct_demorgan itp lit terms = 
  let conjoin l t =
    match follow itp t with
    | Lit m -> 
      if Literal.negate l = m then Disj []
      else if l = m then Lit m
      else Conj [ Lit l; Lit m ]
    | _ -> Conj [ Lit l; t ]
  in
  let distributed = List.map (conjoin lit) terms in
  List.filter (fun t -> follow itp t <> Disj []) distributed

(** distribute single literal using DeMorgan's rule     *)
let disjunct_demorgan itp lit terms = 
  let disjoin l t =
    match follow itp t with
    | Lit m -> 
      if Literal.negate l = m then Conj []
      else if l = m then Lit m
      else Disj [ Lit l; Lit m ]
    | _ -> Disj [ Lit l; t ]
  in
  let distributed = List.map (disjoin lit) terms in
  List.filter (fun t -> follow itp t <> Conj []) distributed

(** simplify a list of terms                            *)
let rec simplify_aux agressive is_conjunction itp terms =
  (* let simplified = List.map (simplify itp (level-1)) terms *)
  (* group literals, disjunctions, conjunctions *)
  let sorted = List.sort (compare is_conjunction itp) terms in
  let rec filter_terms lst =
    if lst = [] then [] 
    else 
      let first = List.hd lst in
      let followed_first = follow itp first in
      let tail = List.tl lst in

      (* detect/discard inconsistencies                 *)
      if is_inconsistent itp followed_first
      then                         (* raise or discard? *)
        (if is_conjunction then raise Inconsistent
         else filter_terms tail)

      (* detect/discard tautologies                     *)
      else if is_tautology itp followed_first
      then                         (* raise or discard? *)
        (if is_conjunction then filter_terms tail
         else raise Tautology)

      else 
        (* inline terms if possible                     *)
        ( match followed_first with
        | Conj prefix when is_conjunction && agressive ->
          filter_terms (List.sort (compare is_conjunction itp) (prefix @ tail)) 
        | Disj prefix when (not is_conjunction) && agressive ->
          filter_terms (List.sort (compare is_conjunction itp) (prefix @ tail)) 

        (* ignore lists of length 1                     *)
        | _ when List.length lst = 1 -> lst

        (* analysis based on first two elements         *)
        | _ ->
          let second = List.hd tail in
          let followed_second = follow itp second in
          
          (match (followed_first, followed_second) with
          (* find contradicting/equivalent literals     *)
          | (Lit l, Lit m) ->
            if (Literal.negate m = l) then 
              (if is_conjunction then 
                  raise Inconsistent
               else 
                  raise Tautology)
            else if (m = l) then filter_terms tail
            else first :: (filter_terms tail)

          (* see whether we can distribute literals     *)
          | (Lit l, Disj terms) when is_conjunction ->
            let reduced = conjunct_demorgan itp l terms in
            let simpl = simplify_aux false false itp reduced in
            simpl :: (filter_terms (List.tl tail))
          | (Lit l, Conj terms) when (not is_conjunction) ->
            let reduced = disjunct_demorgan itp l terms in
            let simpl = simplify_aux false true itp reduced in
            simpl :: (filter_terms (List.tl tail))
              
          (* check for duplicates                       *)
          | _ -> 
            if (followed_first = followed_second) 
            then filter_terms tail
            else first :: (filter_terms tail)))
  in
  try 
    let filtered = filter_terms sorted in
    match filtered with
      [ elem ] -> elem
    | _ when is_conjunction -> Conj filtered
    | _ -> Disj filtered
  with Inconsistent -> Disj []
  | Tautology -> Conj []

(** simplify a term if possible                         *)
let simplify agressive itp term =
  match follow itp term with
    Conj terms -> simplify_aux agressive true itp terms
  | Disj terms -> simplify_aux agressive false itp terms
  | _ -> term
    
(** pretty print a partial interpolant                  *)
let rec output_partial chan itp pi = 
  match pi with
    Lit lit -> ResolutionProof.output_clause chan [ lit ]
  | Part _ -> output_partial chan itp (follow itp pi)
  | Conj [] -> output_char chan 'T'
  | Conj (conj :: conjuncts) -> 
    begin
      output_char chan '('; output_partial chan itp conj;
      List.iter 
        (fun elem -> 
          output_string chan " & ";
          output_partial chan itp elem) 
        conjuncts;
      output_char chan ')';
    end
  | Disj [] -> output_char chan 'F'
  | Disj (disj :: disjuncts) -> 
    begin
      output_char chan '('; output_partial chan itp disj;
      List.iter 
        (fun elem -> 
          output_string chan " | ";
          output_partial chan itp elem) 
        disjuncts;
      output_char chan ')';
    end

(** pretty print an interpolant                         *)
let output_interpolant chan itp = 
  output_partial chan itp (Part itp.root)
  
(********************************************************)  
(* Interpolation functions                              *)

(** Recursively compute partial interpolant for vertex  *)
let rec interpolate_part proof labelling pitp vertex = 
  if not (Hashtbl.mem pitp.pi vertex) then
    let (clause, pivot) = 
      ResolutionProof.get_vertex_label proof vertex in
    match pivot with
      None ->              (* interpolant for leaf node *)
        ( let (prt, lbls) = labelling vertex in
          let intp = 
            if prt = APartition then    (* A-partition? *)
              let lits = (dproject BLocal clause lbls) in
              Disj (List.map (fun l -> Lit l) lits)
            else                        (* B-partition  *)
              let lits = dproject ALocal clause lbls in
              let nlits = List.map (Literal.negate) lits in
              Conj (List.map (fun l -> Lit l) nlits)
          in
          Hashtbl.replace pitp.pi vertex intp; 
          Hashtbl.replace pitp.lmap vertex lbls; )
          
    | Some piv_lit ->     (* interpolant for inner node *)
      let piv_nlit = Literal.negate piv_lit in
      let preds = Graph.predecessors proof.dag vertex in
      List.iter (interpolate_part proof labelling pitp) preds;
      assert (List.length preds = 2);
      let (u, v) = (List.hd preds, List.hd (List.tl preds)) in
      (* which clause contains non-negated pivot?       *)
      let (antecedent, _) = 
        ResolutionProof.get_vertex_label proof u in
      let (vplus, vminus) =
        if (List.mem piv_lit antecedent) then (u, v) else (v, u)
      in
      (* get antecedent interpolants and labellings     *)
      let (itp_plus, lbls_plus) = 
        (Hashtbl.find pitp.pi vplus, 
         Hashtbl.find pitp.lmap vplus)
      in
      let (itp_minus, lbls_minus) = 
        (Hashtbl.find pitp.pi vminus, 
         Hashtbl.find pitp.lmap vminus)
      in
      (* compute label of pivot literal                 *)
      let piv_lbl = 
        merge_labels (lbls_plus piv_lit) (lbls_minus piv_nlit)
      in
      (* construct new partial interpolant              *)
      let intp =
        match piv_lbl with
          Transparent -> raise (Invalid_labelling "Pivot")
        | ALocal -> Disj [Part vplus ; Part vminus]
        | BLocal -> Conj [Part vplus ; Part vminus]
        | Shared -> (* introduce multiplexer over pivot *)
          let pos = Disj [ Lit piv_lit ; itp_plus ] in
          let neg = Disj [ Lit piv_nlit ; itp_minus ] in
          Conj [ simplify false pitp pos ; simplify false pitp neg ]
      in
      (* simplify the interpolant if possible           *)
      let simplified_intp = simplify false pitp intp in 
      (* compute labelling for target clause            *)
      let lbls = merge_label_maps lbls_plus lbls_minus clause in
      (* store results in partial interpolant structure *)
      Hashtbl.replace pitp.pi vertex simplified_intp; 
      Hashtbl.replace pitp.lmap vertex lbls

(** Compute an interpolant for a resolution proof       
    @param proof the resolution refutation
    @param labelling partition and labelling for leafs
*)
let labelled_interpolate proof labelling =
  match proof.sink with
    None -> raise (Invalid_proof "Proof has no root node")
  | Some sink -> let itp = 
                   {
                     pi   = Hashtbl.create sink;
                     lmap = Hashtbl.create sink;
                     root = sink
                   }  
                 in 
                 interpolate_part proof labelling itp sink;
                 itp

(** Compute shared variables of a proof
    @param partitions maps vertex to partition (APartition, BPartition)
    @return a function mapping literals to ALocal/BLocal/Shared
*)
let literal_sharing proof partitions = 
  let var_map = match proof.sink with
      None -> raise (ResolutionProof.Invalid_proof "Missing sink")
    | Some root -> Hashtbl.create root
  in
  let partition_label v =
    if partitions v = APartition then ALocal else BLocal
  in
  ResolutionProof.visit proof 
    (fun v ->
      let (clause, _) = ResolutionProof.get_vertex_label proof v in
      List.iter                   (* visit all literals *)
        (fun lit -> let lit_var = Literal.variable lit in
                    let lbl = 
                      try
                        Hashtbl.find var_map lit_var
                      with Not_found -> Transparent
                    in
                    if lbl <> Shared then 
                      Hashtbl.replace var_map lit_var 
                        (merge_labels lbl (partition_label v)))
        clause)
    (fun _ -> ());              (* ignore inner vertices *)
  (fun lit -> Hashtbl.find var_map (Literal.variable lit))

(** Compute Pudlak's interpolant for a resolution proof       
    @param proof the resolution refutation
    @param partitions maps leafs to partitions
*)
let interpolate_pudlak proof partitions = 
  let lookup_lbls = literal_sharing proof partitions in
  let labelling vertex = (partitions vertex, lookup_lbls) in
  labelled_interpolate proof labelling

(** Compute McMillan's interpolant for a resolution proof       
    @param proof the resolution refutation
    @param partitions maps leafs to partitions
*)
let interpolate_mcmillan proof partitions = 
  let lookup_lbls lit =
    match (literal_sharing proof partitions) lit with
      Transparent -> Transparent
    | ALocal -> ALocal
    | _ -> BLocal
  in
  let labelling vertex = (partitions vertex, lookup_lbls) in
  labelled_interpolate proof labelling

(** Compute McMillan's inverse interpolant 
    @param proof the resolution refutation
    @param partitions maps leafs to partitions
*)
let interpolate_inv_mcmillan proof partitions = 
  let lookup_lbls lit =
    match (literal_sharing proof partitions) lit with
      Transparent -> Transparent
    | BLocal -> BLocal
    | _ -> ALocal
  in
  let labelling vertex = (partitions vertex, lookup_lbls) in
  labelled_interpolate proof labelling

(** Compute "minimal" interpolant 
    @param proof the resolution refutation
    @param partitions maps leafs to partitions
*)
let interpolate_minimal proof partitions = 
  let lookup_lbls vertex lit =
    let lbl = (literal_sharing proof partitions) lit in
    if lbl <> Shared then lbl
    else match partitions vertex with
      APartition -> ALocal
    | BPartition -> BLocal
  in
  output_string stdout "Computing intp\n";
  let labelling vertex = (partitions vertex, lookup_lbls vertex) in
  labelled_interpolate proof labelling
