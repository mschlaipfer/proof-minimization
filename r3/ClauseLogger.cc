#include "ClauseLogger.h"

extern "C" {

#include <caml/memory.h>
#include <caml/alloc.h>
#include <caml/fail.h>

}

using namespace Minisat;

/**
 * Constructor for clause logger.
 * Initialises and registers OCaml values.
 */
ClauseLogger::ClauseLogger () 
{ 
  caml_register_global_root (&ml_clauses);  
  caml_register_global_root (&ml_learnt);  
  ml_clauses = Val_emptylist;
  ml_learnt = Val_emptylist;
}

/**
 * Destructor for clause logger.
 * Deregisters OCaml values.
 */
ClauseLogger::~ClauseLogger () 
{ 
  caml_remove_global_root (&ml_clauses);  
  caml_remove_global_root (&ml_learnt);  
}

/**
 * Returns the current list of clauses
 */
value ClauseLogger::clauses ()
{
  return ml_clauses;
}

/**
 * Returns the current list of learnt clauses
 */
value ClauseLogger::learnt ()
{
  return ml_learnt;
}

/**
 * Logs a MiniSAT clause. 
 * This function converts the clause into its corresponding OCaml
 * representation and adds it to the approprate list (clauses or
 * learnt).
 */
void ClauseLogger::log (const vec<Lit>& ps, bool learnt)
{
  CAMLparam0();
  CAMLlocal2 (ml_head, ml_clause);

  Lit p;
  unsigned i;
  vec<Lit> simp;

  /* remove duplicates from clause */
  for (i = 0, p = lit_Undef; i < ps.size(); ++i)
    if (ps[i] == ~p)
      CAMLreturn0;       /* ignore tautological clauses */
    else if (ps[i] != p) /* assuming that ps is sorted  */
      simp.push (p = ps[i]);

  /* convert the clause into an OCaml data structure */
  ml_clause = caml_alloc_tuple (simp.size());
  for (i = 0; i < simp.size(); ++i)
    Store_field (ml_clause, i, Val_int (simp[i].x));

  /* now add the clause as the head of the appropriate list */
  ml_head = caml_alloc (2, 0);
  Store_field (ml_head, 0, ml_clause);
  if (learnt)
  {
    Store_field (ml_head, 1, ml_learnt);
    ml_learnt = ml_head;
  }
  else
  {
    Store_field (ml_head, 1, ml_clauses);
    ml_clauses = ml_head;
  }
  
  CAMLreturn0;
}

/**
 * Logs a MiniSAT unit clause. 
 */
void ClauseLogger::log (const Lit& lit, bool learnt)
{
  vec<Lit> tmp (1, lit);
  log (tmp, learnt);
}
