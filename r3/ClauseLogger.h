/* Ocaml header files need to be made extern C for linker */
extern "C" {

/* somebody thought #define alloc caml_alloc is a good idea; it's not */
#define CAML_COMPATIBILITY_H

#include <caml/mlvalues.h>

}

/* MiniSAT SolverTypes */
#include <core/SolverTypes.h>

#ifndef __CLAUSE_LOGGER__
#define __CLAUSE_LOGGER__


namespace Minisat {

class ClauseLogger {
public:
  ClauseLogger ();
  void log (const vec<Lit>&, bool);
  void log (const Lit&, bool);
  value clauses ();
  value learnt ();
  ~ClauseLogger ();

private:
  value ml_clauses;
  value ml_learnt;
};

}

#endif
