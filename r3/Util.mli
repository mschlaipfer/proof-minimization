(** A directed graph structure *)
module type GRAPH = 
  sig
    type t
    type vertex = int

    (** generates an empty graph *)
    val create : unit -> t

		(** adds a vertex to the graph
				if a vertex id is provided, 
				it must not clash with existing vertices *)
		val add_vertex : ?id:vertex -> t -> vertex

    (** returns the maximal vertex id *)
    val max_vertex : t -> vertex

    (** adds a directed edge to the graph *)
    val add_edge : t -> vertex -> vertex -> unit

    (** removes a directed edge to the graph *)
    val remove_edge : t -> vertex -> vertex -> unit

    (** @return successors of a vertex *)
    val successors : t -> vertex -> vertex list

    (** @return predecessors of a vertex *)
    val predecessors : t -> vertex -> vertex list
  end

module Graph : GRAPH
