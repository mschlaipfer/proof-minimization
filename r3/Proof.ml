(********************************************************)
(** The Proof module provides formulas and proofs       *)
(** @author Georg Weissenbacher                         *)

open Util 

(********************************************************)
(** Abstract type for formulas used in proofs           *)
module type FORMULA = 
sig
  type t
end
  
(********************************************************)
(** Signature of proofs                                 *)
module type S =
sig
  exception Invalid_proof of string

  type formula
  type vertex = Graph.vertex
  type t = { dag          : Graph.t ; 
						 ell          : (vertex, formula) Hashtbl.t ;
						 mutable sink : vertex option 
					 }
    
  (** generate an empty proof *)
  val create : unit -> t

  (** adds a vertex to the proof
			if a vertex id is provided, 
			it must not clash with existing vertices *)
  val add_vertex : ?id:vertex -> t -> vertex
    
  (** add a label to a vertex *)
  val add_vertex_label : t -> vertex -> formula -> unit

  (** get the label of a vertex *)
  val get_vertex_label : t -> vertex -> formula
    
  (** add a proof step comprising premises and conclusion *)
  val add_step : t -> vertex list -> vertex -> unit

  (** get the predecessors of a node *)
  val get_premises : t -> vertex -> vertex list
    
  (** set sink to a vertex *)
  val set_sink : t -> vertex -> unit

  (** iterate from 0 to max_vertex *)
  val iter: (vertex -> unit) -> t -> unit

  (** visit leaves of proof first, then inner nodes       *)
  val visit : t -> (vertex -> unit) -> (vertex -> unit) -> unit

end

module Make (Formula : FORMULA) : 
  (S with type formula = Formula.t) =
struct
  exception Invalid_proof of string

  type formula = Formula.t
  type vertex = Graph.vertex
  type t = { dag          : Graph.t ; 
						 ell          : (vertex, formula) Hashtbl.t ; 
						 mutable sink : vertex option 
					 }
    
  let create () = 
    {  dag = Graph.create (); 
			 ell = Hashtbl.create 100; 
			 sink = None;
    }

	let add_vertex ?id p =
		match id with
			None -> Graph.add_vertex p.dag
		| Some n -> Graph.add_vertex ~id:n p.dag

  let add_vertex_label p v f = 
    Hashtbl.replace p.ell v f

  let get_vertex_label p v = 
    Hashtbl.find p.ell v
			
  let add_step p (premises : vertex list) (conclusion : vertex) =
    List.iter 
			(function i -> Graph.add_edge p.dag i conclusion) premises
			
  let get_premises p vertex =
    Graph.predecessors p.dag vertex

  let set_sink p v =
    p.sink <- Some v

  let iter f p =
    for i = 0 to Graph.max_vertex p.dag do f(i) done

  (** visit leaves of proof first, then inner nodes       
      @param proof the proof to be visited
      @param f function to be applied to leaves
      @param g function to be applied to inner vertices
  *)
  let visit proof f g = 
    let sink = match proof.sink with
        None -> raise (Invalid_proof "Missing sink")
      | Some v -> v
    in
    let visited = Hashtbl.create sink in
    let workqueue = Queue.create () in
    let rec visit_vertex v =
      if not (Hashtbl.mem visited v) then
        begin
          Hashtbl.add visited v true;
          match (Graph.predecessors proof.dag v) with
            [] -> f v
          | preds -> List.iter visit_vertex preds;
            Queue.add v workqueue 
        end
    in
    visit_vertex sink;
    Queue.iter g workqueue

end



