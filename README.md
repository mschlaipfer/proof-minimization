#Research tool for resolution proof and Craig interpolant minimization

This is a prototype implementation, written mainly in Scala.

##Getting started:

The project can be built using  
  `sbt compile`

and run using  
  `./run.sh <program arguments>`

You can print an overview of supported arguments by running  
  `./run.sh --help .`

An example run could be:  
  `./run.sh mode --min-vars --abc ~/tools/abc/abc --abc-cmds scripts/abc_cmds.txt --dont-suppress r3benchmarks/hwmcc/6s7.aig.proof`


The input proofs can be computed using the produce_proofs scripts in the scripts
directory. Input data for proof computation can be taken from the Hardware Model
Checking Competition 2013 benchmarks [1] and the plain MUS track of the SAT11
competition [2].


##Dependencies:
  * r3 (written by Georg Weissenbacher) is provided, as a tool for benchmark
    creation
  * a modified version of MiniSat 1.14-p is provided for benchmark creation
  * ABC for AIG generation, to evaluate interpolant size (commit hash 738118b,
    as provided at [3])
  * sbt build tool (we use version 0.13.0)
  * Some Scala libraries, specified in build.sbt and project/plugins.sbt

##License:

The overwhelming portion of this software is licensed under the MIT license. The
license text is provided in LICENSE. The parts of the software, which have been
reused or adapted from other projects are marked clearly within the respective
files (typically with a license text) or have accompanying license files
provided. I try to satisfy all the licensing conditions to the best of my
knowledge. If you find a violation, please let me know and I will take care of
it.


[1] http://fmv.jku.at/hwmcc13/  
[2] http://www.cril.univ-artois.fr/SAT11/  
[3] https://bitbucket.org/alanmi/abc  


Please contact me (Matthias Schlaipfer) at <matthias.schlaipfer@tuwien.ac.at>, if
you have questions, or find bugs.
  
