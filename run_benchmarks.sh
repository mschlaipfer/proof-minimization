#!/bin/bash

directory=$1/

csv=$2


sbt one-jar

#echo "$(date);$(git rev-parse HEAD)" >> $csv
echo "$(date);$fwdargs" >> $csv
echo "name;"$(java -Xmx5g -jar ./target/scala-2.10/proof-minimization_2.10-0.1-one-jar.jar --print-header .) >> $csv

for FILE in $( find $directory -maxdepth 1 -type f -name "*.trace" | sort -V )
do
  echo "Running "$FILE

  apart=${FILE%.trace}"_a_ends_at.txt"
  bpart=${FILE%.trace}"_b_ends_at.txt"
  fwdargs=${@:3}
  if [[ -e $apart && -e $bpart ]]; then
    fwdargs=$fwdargs" --a-partition "$apart" --b-partition "$bpart
    result=$(timeout 30m java -Xmx5g -jar ./target/scala-2.10/proof-minimization_2.10-0.1-one-jar.jar $fwdargs $FILE)
    rc=$?
    bn=$(basename -s .trace $FILE)
    if [[ $rc -eq 124 ]]; then
      echo "$bn;t/o;" >> $csv
      echo "Timeout "$FILE
    elif [[ $rc != 0 ]]; then
      echo "$bn;m/o;" >> $csv
      echo "Memout "$FILE
    else
      echo "$bn;$result" >> $csv
      echo "Done "$FILE
    fi
  else
    echo "No partitions available for "$FILE
  fi
done
