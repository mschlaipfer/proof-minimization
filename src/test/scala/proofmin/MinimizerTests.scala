package proofmin

import org.scalatest.FunSuite
import scala.collection.mutable.Map

class MinimizerTests extends FunSuite {

  import TestUtils._

  ////learnts not supported anymore
  //test("subsumption 1") {
  //  val p = createProof
  //  val minimizer = new Minimizer(p)
  //  minimizer.allRm()
  //  minimizer.reconstructProof(false)
  //  minimizer.recomputeAndSubsumption()
  //  val size = p.info
  //  assert(size._1 + size._2 === 11)
  //}

/*
  test("subsumption 2") {
    val p = createProofStrichmanFig3
    val minimizer = new Minimizer(p)
    minimizer.allRm()
    //minimizer.subsumption()
    minimizer.reconstructProof(false)
    val (size, _) = p.info
    assert(size._1 + size._2 === 3)
  }

  test("tautologies") {
    val p = createBetterThanAshutosh
    val minimizer = new Minimizer(p)
    minimizer.allRmAshutosh()
    minimizer.reconstructProof(false)
    val (size, _) = p.info
    assert(size._1 + size._2 === 9)
    assert(p.root.resData.cl.labels.isEmpty)

    val p2 = createBetterThanAshutosh
    val minimizer2 = new Minimizer(p2)
    minimizer2.allRm()
    minimizer2.reconstructProof(false)
    val (size2, _) = p2.info
    assert(size2._1 + size2._2 === 7)
    assert(p2.root.resData.cl.labels.isEmpty)
  }

  //test("minvars 1") {
  //  val p = createSinglePassColoringAware
  //  val minimizer = new Minimizer(p)
  //  minimizer.allRm()
  //  minimizer.reconstructProof(false)
  //  val size = p.info
  //  assert(size._1 + size._2 === 9)
  //  assert(p.root.resData.cl.labels.isEmpty)
  //}

  test("example 1") {
    val p = createExample1
    val minimizer = new Minimizer(p)
    minimizer.allRm()
    //minimizer.subsumption()
    minimizer.reconstructProof(true)
    val (size, _) = p.info
    assert(size._1 + size._2 === 5)
  }

  test("example 6") {
    val p = createExample6
    val minimizer = new Minimizer(p)
    minimizer.allRm()
    minimizer.reconstructProof(false)
    val (size, _) = p.info
    assert(size._1 + size._2 === 12)
    minimizer.recomputeAndSubsumption()
    val (size2, _) = p.info
    assert(size2._1 + size2._2 === 5)
  }
  test("example 7 no suppress") {
    implicit val config = Options.Config(verbose = true, mode = Options.MinVars, suppressSubstitutions = false)

    val p = createExample7a
    //val p = createExample7b
    val minimizer = new Minimizer(p)
    minimizer.allRm()
    minimizer.reconstructProof(false)
    val (size, itp) = p.info
    println(itp)
    assert(size._1 + size._2 === 5)
  }

  test("example 7 suppress") {
    implicit val config = Options.Config(verbose = true, mode = Options.MinVars, suppressSubstitutions = true)

    val p = createExample7a
    //val p = createExample7b
    val minimizer = new Minimizer(p)
    minimizer.allRm()
    minimizer.reconstructProof(false)
    val (size, itp) = p.info
    println(itp)
    assert(size._1 + size._2 === 9)
  }
  */

  test("example 8") {
    implicit val config = Options.Config(verbose = true, mode = Options.MinProof, ashutosh = true)

    val p = createExample8
    //val p = createExample7b
    val minimizer = new Minimizer(p)
    minimizer.allRmAshutosh()
    minimizer.reconstructProof(false)
    val size = (p.initialCount, p.internalCount)
    assert(size._1 === 7)
    assert(size._2 === 6)
  }
}
