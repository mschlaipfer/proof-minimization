package proofmin

object TestUtils {

  implicit val config = Options.Config(verbose = true, recomputeAndSubsumption = true) // default config
  /*

  def createProof = {
    implicit val config = Options.Config(verbose = true, mode = Options.MinVars)
    // resolution contains 12 nodes + 3 others
    // illustration on page 39 in notes
    val p = new Proof(13, 15)
    p.addLeaf(0, Clause(1 -> B, -3 -> B))
    p.addLeaf(1, Clause(2 -> B, 3 -> B, -4 -> B))
    p.addInner(2, 0, 1)
    p.addLeaf(3, Clause(1 -> B, -2 -> B))
    p.addInner(4, 2, 3)
    p.addLeaf(5, Clause(-1 -> B, -4 -> B))
    p.addInner(6, 4, 5)
    p.addLeaf(7, Clause(1 -> B, 3 -> B))

    p.addLeaf(8, Clause(-1 -> A, -2 -> A))
    p.addInner(9, 3, 8)
    p.addLeaf(10, Clause(2 -> A, 4 -> A))
    p.addInner(11, 9, 10)
    p.addLeaf(12, Clause(4 -> A, -1 -> A))

    p.addInner(13, 6, 11)

    // learnt
    p.addInner(14, 4, 10)

    p.initialize(4)
    p
  }

  def createProofStrichmanFig3 = {
    val p = new Proof(6, 7)
    p.addLeaf(0, Clause(1 -> Top, 2 -> Top, 3 -> Top))
    p.addLeaf(1, Clause(-2 -> Top, 4 -> Top))
    p.addInner(2, 0, 1)
    p.addLeaf(3, Clause(-1 -> Top, -2 -> Top, 5 -> Top))
    p.addInner(4, 2, 3)
    p.addLeaf(5, Clause(2 -> Top, 6 -> Top))
    p.addInner(6, 4, 5)

    p.initialize(6)
    p
  }
  
  def id(name: Char) = {
    name match {
      case 'v' => 1
      case 'w' => 2
      case 'p' => 3
      case 's' => 4
      case 't' => 5
      case _ => throw new Exception
    }
  }
  def createBetterThanAshutosh = {
    val p = new Proof(22, 23)
    p.addLeaf(0, Clause(id('p') -> Top, id('w') -> Top, id('s') -> Top))
    p.addLeaf(1, Clause(id('w') -> Top, -id('s') -> Top))
    p.addInner(2, 0, 1)
    p.addLeaf(3, Clause(-id('w') -> Top, id('t') -> Top))
    p.addInner(4, 2, 3)
    p.addLeaf(5, Clause(-id('t') -> Top, id('s') -> Top))
    p.addLeaf(6, Clause(-id('p') -> Top, id('v') -> Top, -id('s') -> Top))
    p.addInner(7, 4, 5)
    p.addInner(8, 4, 6)
    p.addLeaf(9, Clause(-id('s') -> Top, id('p') -> Top))
    p.addInner(10, 9, 7)
    p.addLeaf(11, Clause(-id('p') -> Top, -id('s') -> Top, id('t') -> Top))
    p.addInner(12, 10, 11)
    p.addLeaf(13, Clause(id('s') -> Top, -id('p') -> Top))
    p.addInner(14, 13, 8)
    p.addLeaf(15, Clause(-id('v') -> Top, id('s') -> Top, -id('p') -> Top))
    p.addInner(16, 14, 15)
    p.addInner(17, 16, 12)
    p.addInner(18, 17, 2)
    p.addLeaf(19, Clause(id('s') -> Top, -id('w') -> Top))
    p.addInner(20, 18, 19)
    p.addLeaf(21, Clause(-id('s') -> Top))
    p.addInner(22, 20, 21)
    
    p.initialize(5)
    p
  }
  
  // for distance
  def createSinglePassColoringAware = {
    implicit val config = Options.Config(verbose = true, mode = Options.MinVars)
    val p = new Proof(8, 9)
    p.addLeaf(0, Clause(-id('p') -> B, id('s') -> B, id('w')-> B))
    p.addLeaf(1, Clause(id('p') -> B))
    p.addInner(2, 0, 1)
    p.addLeaf(3, Clause(id('p') -> A, -id('s') -> A))
    p.addInner(4, 2, 3)
    p.addLeaf(5, Clause(-id('w')-> A))
    p.addInner(6, 5, 4)
    p.addLeaf(7, Clause(-id('p') -> A))
    p.addInner(8, 7, 6)
    
    p.initialize(3)
    p
  }
  
  def createExample1 = {
    val p = new Proof(9, 10)
    p.addLeaf(0, Clause(2 -> Top))
    p.addLeaf(1, Clause(-1 -> Top, -2 -> Top, -3 -> Top))
    p.addLeaf(2, Clause(-1 -> Top, 2 -> Top, 3 -> Top))
    p.addLeaf(3, Clause(-1 -> Top, -2 -> Top))
    p.addLeaf(4, Clause(1 -> Top))
    p.addInner(5, 0, 1)
    p.addInner(6, 5, 2)
    p.addInner(7, 3, 4)
    p.addInner(8, 7, 6)
    p.addInner(9, 8, 4)
    
    p.initialize(3)
    p
  }

  def createExample6 = {
    val p = new Proof(11, 12)
    p.addLeaf(0, Clause(-1 -> Top, 2 -> Top))
    p.addLeaf(1, Clause(1 -> Top, 3 -> Top))
    p.addLeaf(2, Clause(-2 -> Top))
    p.addLeaf(3, Clause(-3 -> Top, -4 -> Top))
    p.addLeaf(4, Clause(1 -> Top, 2 -> Top))
    p.addLeaf(5, Clause(-1 -> Top, 4 -> Top))
    p.addInner(6, 0, 1)
    p.addInner(7, 6, 2)
    p.addInner(8, 7, 3)
    p.addInner(9, 4, 5)
    p.addInner(10, 9, 2)
    p.addInner(11, 8, 10)
    
    p.initialize(4)
    p
  }

  def createExample7a(implicit config: Options.Config) = {

    val p = new Proof(8, 9)
    p.addLeaf(0, Clause(1 -> A, -2 -> A))
    p.addLeaf(1, Clause(2 -> A, 3 -> A))
    p.addLeaf(2, Clause(2 -> B, -3 -> B))
    p.addLeaf(3, Clause(-2 -> B))
    p.addLeaf(4, Clause(-1 -> A))
    p.addInner(5, 0, 1)
    p.addInner(6, 5, 2)
    p.addInner(7, 6, 3)
    p.addInner(8, 7, 4)
    
    p.initialize(3)
    p
  }

  def createExample7b(implicit config: Options.Config) = {
    implicit val config = Options.Config(verbose = true, mode = Options.MinVars)

    val p = new Proof(4, 5)
    p.addLeaf(0, Clause(2 -> A, 3 -> A))
    p.addLeaf(1, Clause(2 -> B, -3 -> B))
    p.addLeaf(2, Clause(-2 -> B))
    p.addInner(3, 0, 1)
    p.addInner(4, 3, 2)
    
    p.initialize(3)
    p
  }
  */

  def createExample8(implicit config: Options.Config) = {
    implicit val config = Options.Config(verbose = true, mode = Options.MinProof)

    val n0 = Init(List(1).map(Literals.toPosLitRepresentation(_)), Top)
    val n1 = Init(List(-1, -4, 5).map(Literals.toPosLitRepresentation(_)), Top)
    val n2 = Init(List(-1, 3, 5).map(Literals.toPosLitRepresentation(_)), Top)
    val n3 = Init(List(-1, 2, -5).map(Literals.toPosLitRepresentation(_)), Top)
    val n4 = Init(List(2, 5).map(Literals.toPosLitRepresentation(_)), Top)
    val n5 = Init(List(3, -5).map(Literals.toPosLitRepresentation(_)), Top)
    val n6 = Init(List(-3, 4, -5).map(Literals.toPosLitRepresentation(_)), Top)
    val n7 = Init(List(-2, 5).map(Literals.toPosLitRepresentation(_)), Top)
    val n8 = Init(List(-4, -5).map(Literals.toPosLitRepresentation(_)), Top)

    val n9 = Res.proofInit(n1, n0, Literals.toPosLitRepresentation(1))
    val n10 = Res.proofInit(n8, n9, Literals.toPosLitRepresentation(5))
    val n11 = Res.proofInit(n2, n0, Literals.toPosLitRepresentation(1))
    val n12 = Res.proofInit(n5, n11, Literals.toPosLitRepresentation(5))
    val n13 = Res.proofInit(n6, n12, Literals.toPosLitRepresentation(3))
    val n14 = Res.proofInit(n3, n0, Literals.toPosLitRepresentation(1))
    val n15 = Res.proofInit(n14, n4, Literals.toPosLitRepresentation(5))
    val n16 = Res.proofInit(n7, n15, Literals.toPosLitRepresentation(2))
    val n17 = Res.proofInit(n13, n16, Literals.toPosLitRepresentation(5))
    val n18 = Res.proofInit(n10, n17, Literals.toPosLitRepresentation(4))

    new Proof(n18, 5, scala.collection.mutable.Set(n0, n1, n2, n3, n4, n5, n6, n7, n8), 9, 10)
  }
}
