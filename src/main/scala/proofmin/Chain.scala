/*package proofmin

object Chain {
  def apply(inputClauseId: Int, proof: Proof): Chain = {
    new Chain(inputClauseId, proof)
  }
}

class Chain private (
  val inputClauseId: Int,
  val proof: Proof) {

  val clausePivotPairs = collection.mutable.ArrayBuffer.empty[(Int, Int)]

  var resultNode: Int = -1 

  def addClausePivotPair(pivot: Int, clauseId: Int): Unit = {
    clausePivotPairs += Pair(pivot, clauseId)
  }

  def isStronglyMergeFree: Boolean = {
    assert(proof.isInitialized)

    // TODO negate the literal not the variable
    val pivots = clausePivotPairs.map{case (piv, _) => Literals.neg(piv)}.toSet

    clausePivotPairs.forall{
      case (_, clId) => {
        (proof.nodes(clId).resData.cl.labels.keySet intersect pivots).isEmpty
      }
    }
  }

  override def toString = {
    (proof.nodes(inputClauseId) +: clausePivotPairs.map{case (_, clId) => proof.nodes(clId)}).mkString(", ")
  }



}
*/
