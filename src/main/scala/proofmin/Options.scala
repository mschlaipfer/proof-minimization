package proofmin

import java.io.File
import scala.util.Random

object Options {

  sealed trait Mode
  case object MinVars extends Mode
  case object MinProof extends Mode

  // TODO doesn't scala, max 22 parameters in case class
  case class Config(
    inFile: Option[File] = None,
    aPartitionEnd: Option[Int] = None,
    bPartitionEnd: Option[Int] = None,
    abc: Option[String] = None,
    abcCmds: Option[String] = None,
    verbose: Boolean = false,
    debug: Boolean = false,
    mode: Mode = MinVars,
    printSeed: Boolean = false,
    printHeader: Boolean = false,
    seed: Int = Random.nextInt,
    domMemory: Boolean = false,
    timeLimit: Int = 0,
    ashutosh: Boolean = false,
    //chainAnalysis: Boolean = false,
    recomputeAndSubsumption: Boolean = false,
    prettyPrintStats: Boolean = false,
    suppressSubstitutions: Boolean = true,
    computeFixpoint: Boolean = false,
    // heuristics for subsumption
    bestProofSizeReplacement: Boolean = false,
    distance: Boolean = false,
    smallestClauseSize: Boolean = false)

  val parser = new scopt.OptionParser[Config]("proof-min") {
    head("proof-min", "0.1")
    arg[File]("<file>") action { (x, c) =>
      c.copy(inFile = Some(x))
    } text ("input file, storing the proof to be analyzed")
    opt[File]("a-partition") valueName("<file>") action { (x, c) =>
      val source = scala.io.Source.fromFile(x)
      val aEnd = source.mkString.toInt
      source.close()
      c.copy(aPartitionEnd = Some(aEnd))
    } text ("File specifying a partition")
    opt[File]("b-partition") valueName("<file>") action { (x, c) =>
      val source = scala.io.Source.fromFile(x)
      val bEnd = source.mkString.toInt
      source.close()
      c.copy(bPartitionEnd = Some(bEnd))
    } text ("File specifying b partition")
    opt[String]("abc") valueName ("<path>") action { (x, c) =>
      c.copy(abc = Some(x))
    } text ("if specified, abc circuit size optimization is run")
    opt[String]("abc-cmds") valueName ("<path>") action { (x, c) =>
      c.copy(abcCmds = Some(x))
    } text ("path to abc cmd script")
    opt[Unit]('v', "verbose") action { (_, c) =>
      c.copy(verbose = true)
    } text ("verbose output")
    opt[Unit]("print-header") action { (_, c) =>
      c.copy(printHeader = true)
    } text ("prints the benchmark data header")
    opt[Unit]("print-seed") action { (_, c) =>
      c.copy(printSeed = true)
    } text ("prints the random seed, for reproducability")
    opt[Int]("set-seed") action { (x, c) =>
      c.copy(seed = x)
    } text ("sets the random seed, for reproducability")
    opt[Int]('t', "time-limit") action { (x, c) =>
      c.copy(timeLimit = x)
    } text ("sets a time limit for subsumption search")
    opt[Unit]('a', "ashutosh") action { (_, c) =>
      c.copy(ashutosh = true, timeLimit = 0)
    } text ("Ashutosh's rmPivots algorithm. No further substitution.")
    opt[Unit]('d', "distance") action { (_, c) =>
      c.copy(distance = true, smallestClauseSize = false, bestProofSizeReplacement = false)
    } text ("Take distance into account when replacing.")
    opt[Unit]('p', "proof-size") action { (_, c) =>
      c.copy(distance = false, smallestClauseSize = false, bestProofSizeReplacement = true)
    } text ("Take proof size into account when replacing.")
    opt[Unit]('s', "clause-size") action { (_, c) =>
      c.copy(distance = false, smallestClauseSize = true, bestProofSizeReplacement = false)
    } text ("Take clause size into account when replacing.")
    opt[Unit]("debug") hidden () action { (_, c) =>
      c.copy(debug = true)
    } text ("enables debug mode")
    opt[Unit]("dont-suppress") action { (_, c) =>
      c.copy(suppressSubstitutions = false)
    } text ("Disable suppression of variable-introducing substitutions")
    opt[Unit]("fp") action { (_, c) =>
      c.copy(computeFixpoint = true)
    } text ("Compute fixpoint")
    /*opt[Unit]("chain-analysis") action { (_, c) =>
      c.copy(chainAnalysis = true)
    } text ("Analyse chains")*/
    opt[Unit]("recompute") action { (_, c) =>
      c.copy(recomputeAndSubsumption = true)
    } text ("Recompute freedom before subsumption.")
    opt[Unit]("pretty-print-stats") action { (_, c) =>
      c.copy(prettyPrintStats = true)
    } text ("Pretty print stats.")
    help("help") text ("prints this usage text")
    cmd("mode") children (
      opt[Unit]("min-vars") action { (_, c) =>
        c.copy(mode = MinVars)
      },
      opt[Unit]("min-proof") action { (_, c) =>
        c.copy(mode = MinProof)
      })
  }

}
