package proofmin

import scala.sys.process._
import scala.language.postfixOps
import scala.collection.mutable.ListBuffer
import scala.util.parsing.combinator._
import java.io.File

object Main {

  def runABC(blifPath: String)(implicit config: Options.Config): (Int, Int) = {
    if(config.abc == None || config.abcCmds == None) {
      outln("Missing path to ABC binary or script file. Not using ABC.")
      return (-1, -1)
    }

    import scala.sys.process._
    val cmd = config.abc.get + " -f "+ config.abcCmds.get + " " + blifPath
    val abcOutput = cmd.!!.split("\n").last // Captures the output

    /*
    format:
    interpolant  : i/o =   68/    1  lat =    0  and =   1634  lev =292
    */
    val andsPattern = """and *= *(\d+)""".r
    val andsCount: Int = andsPattern findFirstIn abcOutput match {
      case Some(andsPattern(count)) => count.toInt
      case None => throw new Exception("abc output invalid")
    }

    val levPattern = """lev *= *(\d+)""".r
    val levCount: Int = levPattern findFirstIn abcOutput match {
      case Some(levPattern(count)) => count.toInt
      case None => throw new Exception("abc output invalid")
    }

    (andsCount, levCount)
  }

  def writeToFile(file: java.io.File, s: String) {
    val pw = new java.io.PrintWriter(file)
    try pw.write(s)
    finally pw.close()
  }

  def main(args: Array[String]) {

    Options.parser.parse(args, Options.Config()) map { implicit config =>
      val benchData = new BenchmarkData
      if (config.printHeader) {
        print(benchData.header)
        sys.exit()
      }

      outln("reading proof...")
      val proofParser: ProofParser = new ProofParserTraceCheck
      val proof = StopWatch("parser").time {
        proofParser.read(config.inFile.get)
      }
      assert(proof.root.resData.cl.labels.isEmpty)
      benchData.proofCreationTime = StopWatch("parser").seconds
      outln("reading proof took "+ benchData.proofCreationTime)

      if (config.timeLimit > 0 || config.recomputeAndSubsumption) {
        proof.initAncestorsAndRegisterNodes()
      }

      /*
       *if(config.chainParser && config.chainAnalysis) {
       *  var stronglyMergeFreeCount = 0
       *  var chainCount = 0
       *  proof.chains.foreach{chain => 
       *    if(proof.nodes(chain.resultNode) != null) {
       *      if(chain.isStronglyMergeFree) {
       *        stronglyMergeFreeCount += 1
       *      }
       *      chainCount += 1
       *    }
       *  }
       *  println(f"$stronglyMergeFreeCount / $chainCount (${stronglyMergeFreeCount.toDouble / chainCount}%2.2f) chains are strongly merge-free")
       *  sys.exit()
       *}
       */

      val unminProofSize = (proof.initialCount, proof.internalCount)

      val unminItpFile = File.createTempFile("unminitp", ".blif", new File("."))
      unminItpFile.deleteOnExit()
      if(config.abc != None && config.abcCmds != None) {
        val unminItpString = proof.getInterpolant
        writeToFile(unminItpFile, unminItpString)
      }
      val unminABCData = runABC(unminItpFile.getAbsolutePath())
      benchData.unmin = Some(ProofData(proof.root.resData.interpolantVars.size, unminProofSize, unminABCData))

      val runtime = Runtime.getRuntime()
      benchData.memUsage = (runtime.totalMemory - runtime.freeMemory) / 1e6

      val minimizer = new Minimizer(proof)
      val doSubsumption = (config.timeLimit > 0 && ! config.recomputeAndSubsumption)

      //var currSize: Tuple3[Int, Int, Int] = null
      /* FIXPOINT STUFF
      do {
      */
        //currSize = proof.getInterpolant
        outln("allRmPivots...")
        StopWatch("allRmPivots").time {
          if (config.ashutosh)
            minimizer.allRmAshutosh()
          else
            minimizer.allRm()
        }
        benchData.tautologyCount = minimizer.tautologyCount
        benchData.suppressedSubstitutionsCount = minimizer.suppressedSubstitutionsCount
        benchData.unnecessarySuppressionCount = minimizer.unnecessarySuppressionCount

        outln("did "+ benchData.tautologyCount +" substitutions")
        outln("suppressed "+ benchData.suppressedSubstitutionsCount +" substitutions")
        outln("unnecessarily "+ benchData.unnecessarySuppressionCount)

        outln("took: " + StopWatch("allRmPivots").seconds + "s, found " +
          benchData.tautologyCount +" tautologies")

        if(config.recomputeAndSubsumption) {
          outln("NOT SUPPORTED ANY LONGER")
          /*
          // updating sigma/varsigma after each substitution
          outln("reconstructing proof...")
          StopWatch("reconstructProof").time(minimizer.reconstructProof(false))
          outln("took: " + StopWatch("reconstructProof").seconds + "s")

          outln("general substitutions and sigma recomputation...")
          StopWatch("recomputeAndSubsumption").time(minimizer.recomputeAndSubsumption())
          outln("took: " + StopWatch("recomputeAndSubsumption").seconds + "s")
          */
        } else {
          // normal run
          outln("subsumption computation...")
          StopWatch("subsumption").time(minimizer.subsumption())
          outln("took: " + StopWatch("subsumption").seconds + "s")
          outln("subsumptionCount: " + minimizer.options.size)
          benchData.subsumptionsFound = minimizer.options.size

          outln("reconstructing proof...")
          StopWatch("reconstructProof").time(minimizer.reconstructProof(doSubsumption))
          outln("took: " + StopWatch("reconstructProof").seconds + "s")
        }
      /* FIXPOINT STUFF
      } while(config.computeFixpoint && currSize != proof.info._1)
      */

      val minProofSize = (proof.initialCount, proof.internalCount)

      val minItpFile = File.createTempFile("minitp", ".blif", new File("."))
      minItpFile.deleteOnExit()
      if(config.abc != None && config.abcCmds != None) {
        writeToFile(minItpFile, proof.getInterpolant)
      }
      val minABCData = runABC(minItpFile.getAbsolutePath())
      benchData.min = Some(ProofData(proof.root.resData.interpolantVars.size, minProofSize, minABCData))

      if (config.printSeed)
        println("random seed used for this run: " + config.seed)

      benchData.time = StopWatch("allRmPivots").seconds +
        StopWatch("subsumption").seconds +
        StopWatch("reconstructProof").seconds +
        StopWatch("recomputeAndSubsumption").seconds

      if(config.prettyPrintStats)
        println(benchData.prettyPrint)
      else
        print(benchData)

      if (proof.root.resData.cl.labels.nonEmpty) {
        System.err.println(config.inFile.get.getName())
        System.err.println("\nnot a refutation")
        System.err.println(proof.root.resData.cl.labels)
        throw new Exception
      }
    } getOrElse {
      // arguments are bad, error message will have been displayed
    }
  }

}
