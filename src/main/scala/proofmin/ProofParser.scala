package proofmin

import scala.util.parsing.combinator._

abstract class ProofParser extends RegexParsers {
  def proof: Parser[Proof]
  
  def read(file: java.io.File): Proof = {
    val inputSource = scala.io.Source.fromFile(file)
    val retVal = parse(proof, inputSource.bufferedReader()) match {
      case Success(p, _) => p 
      case Failure(message, _) => throw new Exception("Failure: " + message)
      case Error(message, _) => throw new Exception("Error: " + message)
    }
    inputSource.close()
    retVal
  }
}
