package proofmin

import collection._
import collection.mutable.ArrayBuffer
import scala.annotation.tailrec

class Minimizer(val proof: Proof)(implicit config: Options.Config) {

  //  @throws(classOf[java.util.concurrent.TimeoutException])
  //  def timedRun[F](timeout: Long)(f: => F): F = {
  //
  //    import java.util.concurrent.{ Callable, FutureTask, TimeUnit }
  //
  //    val task = new FutureTask(new Callable[F]() {
  //      def call() = f
  //    })
  //
  //    new Thread(task).start()
  //
  //    task.get(timeout, TimeUnit.MILLISECONDS)
  //  }

  val leavesAndJoins = mutable.Set.empty[Node]

  //   1 replacement per incoming path at leaf/join
  val suppressPath = mutable.BitSet.empty
  val options = mutable.Map.empty[Node, immutable.Vector[(Inner, Boolean, Node)]]
  def subsumption(): Unit = {
    val start = System.currentTimeMillis()
    val it = leavesAndJoins.iterator
    // TODO nicer check in separate thread
    while (it.hasNext && (System.currentTimeMillis() - start) < config.timeLimit * 60 * 1000) {
      val n = it.next() // n is candidate to be substituted
      if (!suppressPath.contains(n.id)) {
        val childNodes = n.getChildren

        for (childNode <- childNodes) {
          val (side, path) = n.freedomPaths(childNode)
          // Set literals in clauses first
          val (merged, order) = n.resData.cl.mergeOrdered(Clause(path))
          //          val merged = n.resData.cl.merge(Clause(path))
          subsumption(n, merged, order, path) match {
            case Some(replacement) => {
              options += (n -> ((childNode, side, replacement) +: options.getOrElse(n, immutable.Vector.empty)))
            }
            case None => ()
          }
        }
      }
    }

  }

  private def subsumption(n: Node, merged: Clause, order: Array[(Int, Label)], path: immutable.Map[Int, Label]): Option[Node] = {

    var retVal: Option[Node] = None
    val assignments = mutable.Set.empty[Int]
    for ((lit, label) <- order) {

      assignments += lit
      val ws: regolic.sat.Vector[Clause] = proof.watchLiterals(lit)
      var j = 0
      var k = 0
      while (j < ws.size) {
        val candidateCl = ws(j)
        j += 1

        if (!candidateCl.getNode.miscData.relevantAncestors.get(n.id)) {
          // look for new watch lit
          var found = false
          val candidateIt = candidateCl.labels.iterator
          while (candidateIt.hasNext && !found) {
            val (candidateLit, candidateLabel) = candidateIt.next

            if (candidateCl.getWatched != candidateLit &&
              !assignments.contains(candidateLit)) {
              candidateCl.setWatched(candidateLit)
              proof.watchLiterals(candidateLit).append(candidateCl)
              found = true
            }
          }

          if(!found && candidateCl.subsumes(merged)) {
            // default is smallest clause size (depending on assignment order
            // of literals), resulting from the nature of the literal watching
            // subsumption
            if((retVal == None) || // if no retVal yet, just use the candidate
                // use smallest overall clause size
                (config.smallestClauseSize && retVal.get.resData.cl.labels.size > candidateCl.labels.size) ||
                //use smallest proof size heuristic
                (config.bestProofSizeReplacement && retVal.get.miscData.proofSize > candidateCl.getNode.miscData.proofSize) ||
                //use label distance heuristic (page 63 in notes)
                (config.distance && retVal.get.resData.cl.distance(Clause(path)) > candidateCl.distance(Clause(path)))) {

              retVal = Some(candidateCl.getNode)

              //suppressPath ++= candidateCl.getNode.miscData.relevantAncestors

              //if(!config.recomputeAndSubsumption)
                //unregisterPath(candidateCl.getNode)
              //if (!config.bestProofSizeReplacement)
                //return retVal
            }
            ws(k) = candidateCl
            k += 1
          }
        } else {
          ws(k) = candidateCl
          k += 1
        }
      }
      ws.shrink(j - k)
    }

    if(retVal != None) {

      if(!config.recomputeAndSubsumption)
        unregisterPath(retVal.get)
      // suppressPath could be handled by removing nodes from
      // leavesAndJoins if they are ancestors, but iterator is not
      // robust
      suppressPath ++= retVal.get.miscData.relevantAncestors.get
    }
    return retVal
  }

  @inline private def allChildrenVisited(n: Node, visited: Set[Int]) = n == proof.root || (n.getChildren.nonEmpty && n.getChildren.forall(c => visited.contains(c.id)))

  var tautologyCount = 0
  var suppressedSubstitutionsCount = 0
  var unnecessarySuppressionCount = 0
  def allRmAshutosh(): Unit = {
    val V = mutable.Set.empty[Int]
    val Q = collection.mutable.Queue[Node](proof.root)

    while (Q.nonEmpty) {
      val t = Q.dequeue

      // t not yet visited, but all children have been visited, so this is the next node we work with
      if (!V(t.id) && allChildrenVisited(t, V)) {


        t match {
          case nInner @ Inner(Some(neg), Some(pos)) => {
            // TODO not visited often enough
            V += nInner.id

            Q.enqueue(neg)
            Q.enqueue(pos)

            val piv = nInner.piv

            if (nInner == proof.root)
              nInner.setFreedom(nInner.resData.cl.labels)
            else {
              nInner.setFreedom(computeFreedom(nInner))
            }

            // Cutting off neg
            if (nInner.getFreedom.contains(nInner.piv)) {
              tautologyCount += 1
              //neg.removeChild(nInner)
              nInner.neg = None
            }
            val negSideFreedom = nInner.getFreedom.
              +((Literals.neg(piv), pos.resData.cl.labels(piv) merge neg.resData.cl.labels(Literals.neg(piv)))).
              -(piv)
            neg.addPath(nInner, false, negSideFreedom)

            // Cutting off pos
            if (nInner.getFreedom.contains(Literals.neg(nInner.piv))) {
              tautologyCount += 1
              //pos.removeChild(nInner)
              nInner.pos = None
            }
            val posSideFreedom = nInner.getFreedom.
              +((piv, neg.resData.cl.labels(Literals.neg(piv)) merge pos.resData.cl.labels(piv))).
              -(Literals.neg(piv))
            pos.addPath(nInner, true, posSideFreedom)

            // no further subsumption, can always set to empty
//TODO commented for debugging
            //nInner.freedomPaths = immutable.Map.empty
            //nInner.setFreedom(immutable.Map.empty)

          }
          case _ => ()
        }
      }

    }
  }

  private val visitedUnregisterPath = mutable.Set.empty[Int]
  private def unregisterPath(n: Node) = {
    val Q = collection.mutable.Queue[Node](n)
    while (Q.nonEmpty) {
      val t = Q.dequeue
      if (!visitedUnregisterPath.contains(t.id)) {
        visitedUnregisterPath += t.id
        // TODO handle this in visitedset, so that we do not even get to this check
        //if(t.resData.cl.getWatched != -1)
          proof.unregisterNode(t)
        t.getChildren.foreach(Q.enqueue(_))
      }
    }
  }

  // C1 \vee x, C2 \vee \ol{x}
  // removing e.g. C1 \vee x leads to propagating \ol{x} further down the
  // proof graph. All clauses, where it hasn't been eliminated yet, cannot
  // be considered for substitution

  // TODO maybe similar handling of visited nodes as in unregisterPath?
  private val visitedUnregisterPathUntil = mutable.Set.empty[Int]
  private def unregisterPathUntil(n: Node, lit: Int): Unit = {
    val Q = collection.mutable.Queue[Node](n)
    while (Q.nonEmpty) {
      val t = Q.dequeue
      if (!visitedUnregisterPath/*Until*/.contains(t.id)) {
        visitedUnregisterPath/*Until*/ += t.id
        proof.unregisterNode(t)
        t.getChildren.filterNot(_.resData.cl.labels.contains(lit)).foreach(Q.enqueue(_))
      }
    }
  }

  def subsumeRes(n: Inner, neg: Node, pos: Node, side: Boolean, V: mutable.Set[Int], Q: mutable.Queue[Node]): Unit = {
    
    val (nodeCutOffCandidate, rLitCutOffCandidate): Pair[Node, Int] = 
      if(side) (pos, n.piv)               else (neg, Literals.neg(n.piv))

    val (nodeOther, rLitOther): Pair[Node, Int] = 
      if(side) (neg, Literals.neg(n.piv)) else (pos, n.piv)
    
    // check if substitution/paths collapse should be made
    // if interpolant at n already contains lit, then substitute
    // if labelling allows it, then substitute
    if (n.getFreedom.contains(rLitOther) &&
      (config.mode == Options.MinProof ||
        (config.mode == Options.MinVars && 
          (!config.distance && (!config.suppressSubstitutions || n.resData.interpolantVars.contains(Literals.abs(rLitOther)) || nodeOther.resData.cl.labels(rLitOther) <= n.getFreedom(rLitOther))) ||
          config.distance && nodeCutOffCandidate.resData.cl.distance(Clause(n.getFreedom)) >= n.resData.cl.distance(Clause(n.getFreedom))))) {

      tautologyCount += 1

      nodeCutOffCandidate.removeChild(n)
      //neg.addFreedomPath(nInner, false, null)
      /* FIXPOINT STUFF
      if(config.recomputeAndSubsumption) {
        n.miscData.relevantAncestors = nodeOther.miscData.relevantAncestors
        if(n.getChildren.size > 1)
          n.miscData.relevantAncestors.get += n.id
      }
      */

      if(side) n.pos = None else n.neg = None

      if(nodeCutOffCandidate.getChildren.isEmpty) {
        rmSubProof(nodeCutOffCandidate, V, Q)
      }
      else if(allChildrenVisited(nodeCutOffCandidate, V)) {
        Q.enqueue(nodeCutOffCandidate)
      }

      // XXX be careful with this
      if (config.timeLimit > 0 && !config.recomputeAndSubsumption) {
        suppressPath ++= nodeOther.miscData.relevantAncestors.get
        unregisterPathUntil(n, rLitOther) 
      }
    } else /* node not cut off */ {

      // incompatible, therefore suppressed
      // TODO distance suppression
      if(n.getFreedom.contains(rLitOther) &&
          config.suppressSubstitutions &&
          config.mode == Options.MinVars &&
          !(nodeOther.resData.cl.labels(rLitOther) <= n.getFreedom(rLitOther))) {
        suppressedSubstitutionsCount += 1

        // TODO check if in final interpolant after proof reconstruction
        
        // simple case, more sophisticated propagation of what is in interpolant
        // in other parts of the proof is necessary (page 195 in notes)
        if(n.resData.interpolantVars.contains(Literals.abs(rLitOther)))
          unnecessarySuppressionCount += 1
      }

      // in the case of a suppressed substitution, this overwrites the previous
      // label in varsigma. (page 195 in notes)
      val freedom = n.getFreedom.
        +((rLitCutOffCandidate, nodeOther.resData.cl.labels(rLitOther) merge
          nodeCutOffCandidate.resData.cl.labels(rLitCutOffCandidate)))

      nodeCutOffCandidate.addPath(n, side, freedom)
    }
  }

  def allRm(): Unit = {
    val V = mutable.Set.empty[Int]
    val Q = collection.mutable.Queue[Node](proof.root)

    while (Q.nonEmpty) {
      val t = Q.dequeue

      // t not yet visited, but all children have been visited, so this is the next node we work with
      if (!V(t.id) && allChildrenVisited(t, V)) {
        V += t.id

        t match {
          case _: Leaf if (t.getChildren.size >= 1) => {
            leavesAndJoins += t
          }
          case nInner @ Inner(Some(neg), Some(pos)) => {
            
            if (nInner == proof.root) { // not a refutation
              nInner.setFreedom(nInner.resData.cl.labels)
            } else {
              nInner.setFreedom(computeFreedom(nInner))
            }

            // Check if all the paths have been replaced, so nInner is essentially cut off
            // Check if all the incoming paths are null (freedom is empty as an optimization)
            assert(nInner == proof.root || nInner.getFreedom.nonEmpty)
            // Cutting off neg
            subsumeRes(nInner, neg, pos, false, V, Q)

            // Cutting off pos
            subsumeRes(nInner, neg, pos, true, V, Q)


            if (nInner.getChildren.size > 1) {
              leavesAndJoins += nInner
            }

            // TODO slows computation down
            // if not a join node
            //if (nInner.getChildren.size <= 1) {
            //  nInner.freedomPaths = immutable.Map.empty
            //  nInner.setFreedom(immutable.Map.empty)
            //}

            Q.enqueue(neg)
            Q.enqueue(pos)
          }
          case _ => throw new Exception("invalid node: "+ t)
        }
      }

    }
  }

  // hard-coded to intersection as meet operator
  private def meet(m1: immutable.Map[Int, Label], m2: immutable.Map[Int, Label]): immutable.Map[Int, Label] = {
    if (m1.isEmpty)
      m2
    else {
      val tmp = m1.keySet intersect m2.keySet
      var retVal = immutable.Map[Int, Label]().withDefaultValue(Bottom)
      for (lit <- tmp) {
        val label = (m1(lit) intersect m2(lit))
        retVal += (lit -> label)
      }
      retVal
    }
  }
  private def computeFreedom(n: Node): immutable.Map[Int, Label] = {
    var retVal = immutable.Map.empty[Int, Label].withDefaultValue(Bottom)
    if (n.freedomPaths.size >= 1) {
      val (child, (side, path)) = n.freedomPaths.head
      val rlit = if (side) child.piv else Literals.neg(child.piv)
      retVal = path + (rlit -> n.resData.cl.labels(rlit))

      if (n.freedomPaths.size > 1) {
        for ((child, (side, path)) <- n.freedomPaths.tail) {
          val rlit = if (side) child.piv else Literals.neg(child.piv)
          val updatedPath = path + (rlit -> n.resData.cl.labels(rlit))
          retVal = meet(retVal, updatedPath)
        }
      }
    }
    retVal
  }

  def reconstructProof(doSubsumption: Boolean): Unit = {
    var reducedInitialCount = 0
    var reducedInternalCount = 0
    
    val visitedReconstruct = mutable.Set.empty[Int]
    val Q = collection.mutable.Queue[Node](proof.root)
    val S = collection.mutable.Stack[Inner]()
    while (Q.nonEmpty) {
      val n = Q.dequeue
 
      // if Ashutosh minimization: Not caring about order seems fine
      if (!visitedReconstruct(n.id) && (config.ashutosh || allChildrenVisited(n, visitedReconstruct))) {
        visitedReconstruct += n.id
        // If replaced paths and target match, then n is essentially removed
        // and we don't need to rewrite the proof of n
        var pathsReplaced = 0
        var target = -1
        // Do subsumption for n
        if (doSubsumption && (n.isInstanceOf[Leaf] || n.getChildren.size > 1)) {
          target = n.getChildren.size
          if (options.contains(n)) {
            for ((childNode, side, replacement) <- options(n)) {
              // prevent multiple replacements per path
              if (side) {
                childNode.pos = Some(replacement)
              } else {
                childNode.neg = Some(replacement)
              }
              replacement.addChild(childNode, side)
              /* FIXPOINT STUFF
              if(config.recomputeAndSubsumption) {
                if(replacement.getChildren.size == 2) {
                  replacement.miscData.relevantAncestors.get += replacement.id
                  replacement.getChildren.foreach{c => 
                    c.miscData.relevantAncestors.get += replacement.id
                  }
                }
              }
              */
              outln("replacing " + n)
              outln("with " + replacement)

              pathsReplaced += 1
            }
          }
        }

        if (target != pathsReplaced) {
          n match {
            case Inner(Some(neg), Some(pos)) => {
              Q.enqueue(neg)
              Q.enqueue(pos)
            }
            case Inner(Some(neg), None) => {
              Q.enqueue(neg)
            }
            case Inner(None, Some(pos)) => {
              Q.enqueue(pos)
            }
            case _: Leaf => {
              reducedInitialCount += 1 // all reachable initial vertices are in unsat core
            }
            case _ => throw new Exception("invalid node: "+ n)
          }
        }

        n match {
          case _: Leaf | Inner(None, None) => ()
          case inner: Inner => {
            S.push(inner)
          }
        }
      }
    }

    var newRootFound = false
    while(S.nonEmpty && !newRootFound) {
      val n = S.pop
      // TODO what about removed paths being visited?
      assert(n == proof.root || n.getChildren.nonEmpty)
      val retVal = restoreRes(n)
      reducedInternalCount += retVal._1
      newRootFound = retVal._2
    }
    proof.initialCount = reducedInitialCount
    proof.internalCount = reducedInternalCount
  }

  // removes children and paths starting at n
  // make sure that n is dangling, such that no valid paths are removed
  // TODO OPTIMIZATION: remove nodes from proof
  private def rmSubProof(n: Node, allRmVisited: mutable.Set[Int], allRmQueue: mutable.Queue[Node]) {
    assert(n.getChildren.isEmpty)
    val removeQ = mutable.Queue[Node](n)

    while(removeQ.nonEmpty) {
      val t = removeQ.dequeue
      //if(!allRmVisited.contains(t.id)) {
        //allRmVisited += t.id
        // don't use t again
        // TODO: we could use t, but we would need to keep its proof
        proof.unregisterNode(t)

        t match {
          case inner @ Inner(Some(neg), Some(pos)) => {
            neg.removeChild(inner)
            if(neg.getChildren.size == 0)
              removeQ.enqueue(neg)
            else if(allChildrenVisited(neg, allRmVisited)) {
              allRmQueue.enqueue(neg)
            }
            inner.neg = None

            pos.removeChild(inner)
            if(pos.getChildren.size == 0)
              removeQ.enqueue(pos)
            else if(allChildrenVisited(pos, allRmVisited)) {
              allRmQueue.enqueue(pos)
            }
            inner.pos = None
          }
          case inner @ Inner(Some(neg), None) => {
            neg.removeChild(inner)
            if(neg.getChildren.size == 0)
              removeQ.enqueue(neg)
            else if(allChildrenVisited(neg, allRmVisited)) {
              allRmQueue.enqueue(neg)
            }
            inner.neg = None
          }
          case inner @ Inner(None, Some(pos)) => {
            pos.removeChild(inner)
            if(pos.getChildren.size == 0)
              removeQ.enqueue(pos)
            else if(allChildrenVisited(pos, allRmVisited)) {
              allRmQueue.enqueue(pos)
            }
            inner.pos = None
          }
          // has been removed already, stop here
          case Inner(None, None) => ()
          case _: Leaf => ()
          case _ => throw new Exception("invalid node")
        }
      }
    //}
  }

  // pull down vertex information when restoring a resolution step
  // if necessary, remove one of the ancestors
  // n is the node where information is pulled to
  private def pullDown(n: Inner, remainingSide: Boolean, remove: Boolean): Boolean = {
    val (remainingAnc, removedAnc) = if(remainingSide) {
      (n.pos.get, n.neg.getOrElse(null))
    } else {
      (n.neg.get, n.pos.getOrElse(null))
    }
    remainingAnc.removeChild(n)
    /* FIXPOINT STUFF
    // XXX crashed when n had been removed in unregisterPathUntil already
    proof.unregisterNode(n)
    */

    val childNodes = n.getChildrenWithSideInformation.clone // in order to safely remove child nodes later
    // new root found
    if(childNodes.isEmpty) {
      assert(remainingAnc.resData.cl.labels.isEmpty)
      proof.root = remainingAnc
      return true
    } else {
      /* FIXPOINT STUFF
      if(config.recomputeAndSubsumption) {
        if(childNodes.size > 1)
          remainingAnc.miscData.relevantAncestors.get += remainingAnc.id
      }
      */

      for ((childNode, side) <- childNodes) {
        move(childNode, side, n, remainingAnc)

        /* FIXPOINT STUFF
        if(config.recomputeAndSubsumption) {
          childNode match {
            case Inner(Some(neg), Some(pos)) => { 
              childNode.miscData.proofSize = neg.miscData.proofSize + pos.miscData.proofSize + 1

              childNode.miscData.relevantAncestors = Some(neg.miscData.relevantAncestors.get ++ pos.miscData.relevantAncestors.get)
              if(childNode.getChildren.size > 1)
                childNode.miscData.relevantAncestors.get += childNode.id
            }
            case _ => () // will be updated later
          }
        }
        */
      }
    }

    if(remove) {
      removedAnc.removeChild(n)
      if(removedAnc.getChildren.isEmpty) {
        rmSubProof(removedAnc, mutable.Set.empty[Int], mutable.Queue.empty[Node])
      }
    }


    false
  }

  /*
   * Restores a resolution step, by 
   *  # moving a node to another parent
   *    - 1 parent was already cut off
   *    - 1 parent doesn't contain the rlit
   *    - both parents don't contain the rlit
   *  # recomputing the resolution
   *
   * Returns true if a new root has been found
   */
  private def restoreRes(inner: Inner): (Int, Boolean) = {
    var validResolutionCount = 0
    val newRootFound = if (inner.neg == None) {
      pullDown(inner, true, false) // path was already removed
    } else if (inner.pos == None) {
      pullDown(inner, false, false) // path was already removed
    } else {
      val neg = inner.neg.get
      val pos = inner.pos.get
      if (neg.resData.cl.labels.contains(Literals.neg(inner.piv)) &&
          pos.resData.cl.labels.contains(inner.piv)) {

        /* FIXPOINT STUFF
        if(inner != proof.root) {
          proof.unregisterNode(inner)
        }
        */

        // We might have to "pull down" vertices to get to this point, but
        // all of these resolution steps are in the reduced proof
        inner.resData = Res(neg, pos, inner.piv)
        validResolutionCount += 1

        /* FIXPOINT STUFF
        // only do the update when there is a chance that we will reuse the
        // clause in a subsumption computation
        if(config.recomputeAndSubsumption) {
          inner.miscData.proofSize = pos.miscData.proofSize + neg.miscData.proofSize + 1

          inner.miscData.relevantAncestors = Some(pos.miscData.relevantAncestors.get ++ neg.miscData.relevantAncestors.get)
          if(inner.getChildren.size > 1)
            inner.miscData.relevantAncestors.get += inner.id
        }
        */

        if(inner.resData.cl.labels.nonEmpty) {
          proof.registerNode(inner)
          false
        } else if(inner.resData.cl.labels.isEmpty && inner != proof.root) {
          // new root found
          // remove proof below new root
          val oldRoot = proof.root
          proof.root = inner
          
          val childNodes = proof.root.getChildrenWithSideInformation.clone
          for((childNode, side) <- childNodes) {
            if(side)
              childNode.pos = None
            else
              childNode.neg = None
            proof.root.removeChild(childNode)
            /* FIXPOINT STUFF
            proof.unregisterNode(childNode)
            */
          }
          rmSubProof(oldRoot, mutable.Set.empty[Int], mutable.Queue.empty[Node])

          true
        } else {
          false // old root reached again
        }
      } else if (pos.resData.cl.labels.contains(inner.piv)) {
        pullDown(inner, false, true)
      } else if (neg.resData.cl.labels.contains(Literals.neg(inner.piv))) {
        pullDown(inner, true, true)
      } else {
        // TODO OPTIMIZATION think about heuristic, take lit colors into account
        // maybe proof size is better
        if (neg.resData.cl.labels.size <= pos.resData.cl.labels.size) {
          pullDown(inner, false, true)
        } else {
          pullDown(inner, true, true)
        }
      }
    }

    (validResolutionCount, newRootFound)
  }

  private def move(childNode: Inner, side: Boolean, from: Node, to: Node): Unit = {
    assert(childNode != to)
    if (side) {
      childNode.pos = Some(to)
    } else {
      childNode.neg = Some(to)
    }
    to.addChild(childNode, side)
    from.removeChild(childNode)
  }

  /* FIXPOINT STUFF
  private def updateFreedom(n: Inner, oldFreedom: immutable.Map[Int, Label], side:
    Boolean, update: Boolean): Unit = {
    val (ancestor, rlit) = if(side)
      (n.pos.get, n.piv)
    else
      (n.neg.get, Literals.neg(n.piv))

    val newFreedom = if(n.pos != None && n.neg != None) {
      oldFreedom.+(rlit ->
        (n.pos.get.resData.cl.labels(n.piv) merge n.neg.get.resData.cl.labels(rlit)))
    } else oldFreedom

    val newPs = ??? // TODO

    if(update)
      ancestor.setPath(n, side, newFreedom, newPs)
    else
      ancestor.addPath(n, side, newFreedom, newPs)
  }

  //// Helper function
  //// descendents of n including n
  //private def desc(n: Node): Set[Node] = {
  //  val Q = mutable.Queue[Node](n)
  //  val retVal = mutable.Set.empty[Node]

  //  while(Q.nonEmpty) {
  //    val t = Q.dequeue
  //    retVal += t

  //    t.getChildren.foreach{c => 
  //      Q.enqueue(c)
  //    }
  //  }
  //  retVal
  //}

  // Search for replacements at the outgoing edges of n
  def substitute(n: Node, V: mutable.Set[Int], Q: collection.mutable.Queue[Node]): Unit = {
    val replacements = mutable.Set.empty[Node]
    val childNodes = n.getChildrenWithSideInformation.clone // in order to safely remove child nodes later
    for ((childNode, _) <- childNodes) {
      val (side, path) = n.freedomPaths(childNode)
      val (merged, order) = n.resData.cl.mergeOrdered(Clause(path))
      subsumption(n, merged, order, path) match {
        case Some(replacement) => {
          assert(replacement != childNode)

          replacement match {
            case Inner(Some(neg), Some(pos)) => {
              Q.enqueue(neg)
              Q.enqueue(pos)
            }
            case _: Leaf => ()
            case _ => assert(false)
          }

          replacements += replacement
          move(childNode, side, n, replacement)
        }
        case None => {}
      }
    }
    
    // remove paths if n now dangling
    if(n.getChildren.isEmpty) {
      rmSubProof(n, V, Q)
    }
    
    if(replacements.isEmpty) {
      n match {
        case Inner(Some(neg), Some(pos)) => {
          Q.enqueue(neg)
          Q.enqueue(pos)
        }
        case _: Leaf => ()
        case _ => assert(false)
      }
    }
    for(r <- replacements) {
      r match {
        case Inner(Some(neg), Some(pos)) => {
          if(r.getChildren.size > 1)
            r.miscData.relevantAncestors.get += r.id
        }
        case _: Leaf => ()
        case _ => throw new Exception("invalid replacement "+ r)
      }
    }
  }

  def recomputeFreedom(): Unit = {
    val freedomUpdateQ = collection.mutable.Queue[Node](proof.root)
    val freedomUpdateV = collection.mutable.Set.empty[Int]
    while(freedomUpdateQ.nonEmpty) {
      val t = freedomUpdateQ.dequeue

      // we don't need to visit tangling nodes
      if (!freedomUpdateV(t.id) && allChildrenVisited(t, freedomUpdateV)) {
        freedomUpdateV += t.id
        val freedom = computeFreedom(t)
        t match {
          case inner @ Inner(Some(neg), Some(pos)) => {
            updateFreedom(inner, freedom, false, true)
            updateFreedom(inner, freedom, true, true)
            freedomUpdateQ.enqueue(neg)
            freedomUpdateQ.enqueue(pos)
          }
          case inner @ Inner(Some(neg), None) => {
            assert(false)
            //updateFreedom(inner, freedom, false, true)
          }
          case inner @ Inner(None, Some(pos)) => {
            assert(false)
            //updateFreedom(inner, freedom, true, true)
          }
          case _ => ()
        }
      }
    }
  }

  def substituteAndReconstruct(n: Node, V: mutable.Set[Int], Q: collection.mutable.Queue[Node]) = {
    assert(n.isInstanceOf[Leaf] || n.getChildren.size > 1)

    // 1) move incoming paths to n if possible
    substitute(n, V, Q)

    // 2) reconstruct the proof
    // just trying to reconstruct a part of the proof didn't work out
    reconstructProof(false)
    assert(proof.root.resData.cl.labels.isEmpty)

    // 3) recompute freedom for the whole proof
    // TODO OPTIMIZATION just the part that is necessary
    // substitution + reconstruction
    recomputeFreedom()

  }


  def recomputeAndSubsumption(): Unit = {
    val V = mutable.Set.empty[Int]
    val Q = collection.mutable.Queue[Node](proof.root)
    // remember the path we have walked so far
    // after restoring the resolution steps, walk the same path to update freedom

    while (Q.nonEmpty) {
      val t = Q.dequeue

      // t not yet visited, but all children have been visited, so this is the next node we work with
      if (!V(t.id) && allChildrenVisited(t, V)) {

        V += t.id
        t match {
          case n: Leaf => {
            substituteAndReconstruct(n, V, Q)
          }
          case n: Inner => {
            if (n.getChildren.size > 1) {
              substituteAndReconstruct(n, V, Q)
            }

            if(n.isInstanceOf[Inner]) {
              val nInner = n.asInstanceOf[Inner]
              val freedom = computeFreedom(nInner)
              if(nInner.neg != None) {
                updateFreedom(nInner, freedom, false, false)
                Q.enqueue(nInner.neg.get)
              } else {
                //throw new Exception("nInner.neg == None: "+ nInner)
              }
              if(nInner.pos != None) {
                updateFreedom(nInner, freedom, true, false)
                Q.enqueue(nInner.pos.get)
              } else {
                //throw new Exception("nInner.pos == None: "+ nInner)
              }
              
            }
            // if not a join node

            // TODO OPTIMIZATION resetting this should be fine?
            // slows computation down
            //if (n.getChildren.size <= 1) {
              //n.freedomPaths = immutable.Map.empty
              //n.setFreedom(immutable.Map.empty)
            //}

          }
          case _ => ()
        }
      }

    }
  }
  */

}
