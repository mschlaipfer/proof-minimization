//The MIT License (MIT)

//Copyright (c) 2013 Regis Blanc

//Permission is hereby granted, free of charge, to any person obtaining a copy
//of this software and associated documentation files (the "Software"), to deal
//in the Software without restriction, including without limitation the rights
//to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
//copies of the Software, and to permit persons to whom the Software is
//furnished to do so, subject to the following conditions:

//The above copyright notice and this permission notice shall be included in
//all copies or substantial portions of the Software.

//THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
//IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
//FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
//AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
//LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
//OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
//THE SOFTWARE.

package proofmin

import scala.collection.mutable.HashMap

/*
 * would be cool to have a notion of nested stopwatch, so that the percent
 * could make more sense
 */

object StopWatch {

  private val stopWatches: HashMap[String, StopWatch] = new HashMap

  def apply(tag: String): StopWatch = stopWatches.get(tag) match {
    case Some(sw) => sw
    case None => {
      val sw = new StopWatch(tag)
      stopWatches(tag) = sw
      sw
    }
  }

  //need to handle "nested" timer
  def percents: Map[String, Double] = {
    val total: Double = stopWatches.foldLeft(0d)((a, p) => a + p._2.seconds)
    stopWatches.map(p => (p._1, p._2.seconds/total*100)).toMap
  }

}

class StopWatch private (tag: String) {

  private var elapsed: Long = 0

  def time[A](code: => A): A = {
    val timeBegin = System.nanoTime
    val res: A = code
    val timeElapsed = System.nanoTime - timeBegin
    elapsed += timeElapsed
    res
  }

  def seconds: Double = elapsed/1e9
  def nano: Long = elapsed

  def reset() {
    elapsed = 0
  }

}
