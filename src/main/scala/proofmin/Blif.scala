package proofmin

class Blif(val inputs: collection.mutable.Set[String], val output: String) {

  private var file = ".model interpolant\n"+
    inputs.map(in => in).mkString(".inputs ", " ", "\n") +
    ".outputs "+ output + "\n"

  def addFormula(formula: String): Unit = {
    file += formula + "\n"
  }

  override def toString: String = file +".end"

}
