package proofmin

import scala.collection.mutable.HashMap

object Id {

  private val ids = HashMap.empty[String, Int]

  def fresh(tag: String): Int = ids.get(tag) match {
    case Some(id) => {
      ids(tag) += 1
      ids(tag)
    }
    case None => {
      ids(tag) = 0
      0 
    }
  }

  def value(tag: String) = ids.getOrElse(tag, 0)
  
  def reset(tag: String) = ids -= tag
}
