/* package proofmin

import scala.util.Random

class ChainParser(seed: Int)(implicit config: Options.Config) extends Parser {

  private val ids = collection.mutable.Map.empty[Int, Int]

  private var maxVarId = 0
  private val rnd = new Random(seed)
  private def getLabel = {
    def getRandomLabel = {
      if (Node.idCounter < randomPartition) A
      else B
    }
    config.mode match {
      case Options.MinVars => getRandomLabel
      case Options.MinProof => Top
    }

  }

  private def countLeaves(xs: Array[String]): Int = {
    var numLeaves = 0
    var i = 0
    while (i < xs.size) {
      val split = xs(i).split(" ")

      split match {
        case Array(_, "ROOT", _*) => numLeaves += 1
        case _ => ()
      }
      i += 1
    }
    numLeaves
  }

  private var randomPartition = 0
  private def computeRandomPartition(numLeaves: Int): Unit = {
    randomPartition = rnd.nextInt(numLeaves >> 1) + (numLeaves >> 2) + 1
  }

  private def inner(p: Proof, first: Int, second: Int): (Int, Int, Int) = {
    val freshId = Node.getId // these inner ids aren't stored in the id map, as they are inside a chain.
    p.addInner(freshId, first, second)

    (freshId, first, second)
  }

  private def parse(input: String, p: Proof): Unit = {
    val split = input.split(" ")

    split match {
      case Array(id, "CHAIN", _*) => {
        // Chain
        // 7336 CHAIN 3207 3974 3209
        assert(split.size >= 5)


        var firstClause = split(2).toInt
        var pivot = split(3).toInt
        var secondClause = split(4).toInt
        var (freshId, firstId, secondId) = inner(p,
          ids.getOrElse(firstClause, { val freshId = Node.getId; ids += (firstClause -> freshId); freshId }),
          ids.getOrElse(secondClause, { val freshId = Node.getId; ids += (secondClause -> freshId); freshId }))
        
        val chain = Chain(firstId, p)

        chain.addClausePivotPair(pivot, secondId)

        var i = 5
        while (i < split.size) {
          firstClause = freshId
          pivot = split(i).toInt
          secondClause = ids.getOrElse(split(i+1).toInt, { val freshId = Node.getId; ids += (secondClause -> freshId); freshId })
          val tmp = inner(p, firstClause, secondClause)
          freshId = tmp._1
          secondId = tmp._3

          chain.addClausePivotPair(pivot, secondId)
          i += 2
        }
        ids += (id.toInt -> freshId)

        chain.resultNode = freshId
        p.chains += chain
      }
      case Array(id, "ROOT", _*) => {
        // Clause 
        // 7334 ROOT 409 423
        assert(split.size >= 3)

        val freshId = Node.getId
        ids += (id.toInt -> freshId)

        val label = getLabel
        var labelledLits = collection.immutable.Map.empty[Int, Label]
        var i = 2
        while (i < split.size) {
          val lit = Literals.toPosLitRepresentation(split(i).toInt)
          if (maxVarId < Literals.toVar(lit))
            maxVarId = Literals.toVar(lit)
          labelledLits += (lit -> label)
          i += 1
        }
        p.addLeaf(freshId, Clause(labelledLits))
      }
      case _ => {}
    }
  }

  private def convertLines(xs: Array[String], p: Proof): Unit = {
    if (config.mode == Options.MinVars) {
      val numLeaves = countLeaves(xs)
      computeRandomPartition(numLeaves)
    }

    xs.foreach { x =>
      parse(x, p)
    }
  }

  private def parseProofInfo(lastLine: String): (Int, Int, Int) = {
    val split = lastLine.split(" ")
    (split(1).toInt, split(3).toInt, split(5).toInt)
  }

  def readProof(input: Array[String]): Proof = {
    val (_, nodeCount, chainCount) = parseProofInfo(input.last)
    val p = new Proof(nodeCount - 1, nodeCount)
    convertLines(input, p)
    p.initialize(maxVarId)
    p
  }

}
*/
