package proofmin

object ProofData {

  def header(name: String) =
    s"#vars ($name)" ::
    s"#leafs ($name)" :: 
    s"#internal ($name)" ::
    s"proof size ($name)" ::
    s"#ands ($name)" ::
    s"#levs ($name)" :: Nil
}

case class ProofData(varCount: Int, nodeCounts: (Int, Int), abcData: (Int, Int)) {
  val (leafs, internals) = nodeCounts
  val proofSize = leafs + internals
  val (ands, levs) = abcData

  def stats = 
    s"$varCount" ::
    s"$leafs" :: 
    s"$internals" ::
    s"${leafs + internals}" ::
    s"$ands" ::
    s"$levs" :: Nil
}

class BenchmarkData(implicit val config: Options.Config) {
  var unmin: Option[ProofData] = None
  var min: Option[ProofData] = None
  val seed: Int = config.seed
  var proofCreationTime: Double = 0
  var time: Double = 0
  var memUsage: Double = 0

  var subsumptionsFound = 0
  var tautologyCount = 0
  var suppressedSubstitutionsCount = 0
  var unnecessarySuppressionCount = 0

  var leafsAfterAllRm = 0
  var internalAfterAllRm = 0

  def varsImprovement = (1.0 -
    (min.get.varCount.toDouble / unmin.get.varCount)) * 100
  def sizeImprovement = (1.0 -
    (min.get.proofSize.toDouble / unmin.get.proofSize)) * 100
  def leafsImprovement = (1.0 -
    (min.get.leafs.toDouble / unmin.get.leafs)) * 100
  def internalImprovement = (1.0 -
    (min.get.internals.toDouble / unmin.get.internals)) * 100

  def stats = 
    unmin.get.stats :::
    min.get.stats :::
    s"$leafsAfterAllRm" ::
    s"$internalAfterAllRm" ::
    f"$varsImprovement%2.2f" ::
    f"$sizeImprovement%2.2f" ::
    f"$leafsImprovement%2.2f" ::
    f"$internalImprovement%2.2f" ::
    s"$subsumptionsFound" ::
    s"$tautologyCount" ::
    s"$suppressedSubstitutionsCount" ::
    s"$unnecessarySuppressionCount" ::
    s"$seed" ::
    f"$proofCreationTime%2.2f" ::
    f"$time%2.2f" ::
    f"$memUsage%2.2f" :: Nil

  override def toString = {
    if (unmin != None && min != None) {
      stats mkString(";")
    } else
      throw new Exception("Either min or unmin not set.")
  }

  def prettyPrint = {
    if (unmin != None && min != None) {
      (header zip stats map {case (e1, e2) => e1 +": "+e2}) mkString("Stats:\n", "\n", "")
    } else
      throw new Exception("Either min or unmin not set.")
  }
  

  val header =
    ProofData.header("unmin") :::
    ProofData.header("min") :::
    "leafs after allRm (overapproximation)" ::
    "internal after allRm (overapproximation)" ::
    "vars improvement (%)" ::
    "size improvement (%)" ::
    "leafs improvement (%)" ::
    "internal improvement (%)" ::
    "subsumptions" ::
    "tautologies" ::
    "suppressed substitutions" ::
    "unnecessarily suppressed" ::
    "random seed" ::
    "proof creation time (s)" ::
    "minimization time (s)" ::
    "initial mem usage (MB)" :: Nil

}
