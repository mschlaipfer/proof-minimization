/**
* This file is heavily based on the TraceCheck parser implementation of Skeptik
* (https://github.com/Paradoxika/Skeptik). Therefore this file is licensed under
* the following license:
* Attribution-NonCommercial-ShareAlike 3.0 Unported (CC BY-NC-SA 3.0)
*
* The complete document can be found at:
* http://creativecommons.org/licenses/by-nc-sa/3.0/legalcode
*/


package proofmin

import scala.util.parsing.combinator._
import collection.mutable.{HashMap => MMap, HashSet => MSet}
import collection.immutable.{HashMap => Map, HashSet => Set}
import collection._
//import scala.collection.mutable.ArrayBuffer

/**
* The syntax of a trace is as follows:
*
* <trace> = { <clause> }
* <clause> = <pos> <literals> <antecedents>
* <literals> = "*" | { <lit> } "0"
* <antecedents> = { <pos> } "0"
* <lit> = <pos> | <neg>
* <pos> = "1" | "2" | .... | <max-idx>
* <neg> = "-"<pos>
*
*/

class ProofParserTraceCheck(implicit val config: Options.Config) extends ProofParser with TraceCheckParsers


/**
* Node and clause used interchangably
*/
trait TraceCheckParsers 
extends JavaTokenParsers with RegexParsers {

  implicit val config: Options.Config

  private val varIds = MMap.empty[Int, Int]
  private val clauseIds = MMap.empty[Int, (List[Int],List[Int])]
  private val clauses = MMap.empty[Int, Node]
  var maxClause = 0

  var numInitClauses = -1

  private var initialCount = 0
  private var internalCount = 0

  def proof: Parser[Proof] = rep(clause) ^^ { clIdList =>
    val maxClauseId = clIdList.last
    //computeProof(maxClauseId)
    val root = getNode(maxClauseId)
    new Proof(root, varIds.size, Init.initialVertices, initialCount, internalCount)
  }

  def clause: Parser[Int] = pos ~ literals ~ antecedents ^^ {
    case ~(~(id, litList), antList) => {
      if (litList.isEmpty && antList.isEmpty) {
        throw new Exception("Invalid input at " + id + " ~ " + litList)
      } else {
        clauseIds += (id -> (litList, antList))
        if (maxClause < id) {
          maxClause = id
        }
        if(antList.isEmpty) {
          numInitClauses += 1
        }

        maxClause
      }
    }
    case wl => throw new Exception("Wrong line " + wl)
  }
  
  /**
  * Resolves the clauses represented by a list of indices in the correct order.
  *
  * It does this by keeping track of in which clauses variables occur positively/negatively.
  * This method only initializes these maps and calls the recursive method res with them.
  */
  def resolveClauses(antIds: List[Int]): Node = {
    //println("entering resolveClauses "+ antIds.mkString(", "))
    //map denoting that variable v (not v, resp.) occurs in {clause_1,...,clause_n}
    val posOc = MMap.empty[Int, MSet[Node]]
    val negOc = MMap.empty[Int, MSet[Node]]

    //initialize the maps
    antIds.foreach(id => {
      val clause = getNode(id)
      //println("id: "+ id)
      //println("clause: "+ clause)
      clause.resData.cl.labels.foreach{
        case (l, _) => {
          val v = Literals.toVar(l)

          if(Literals.isNeg(l)) {
            if (negOc.isDefinedAt(v)) {
              negOc(v) += clause
            } else {
              negOc += (v -> MSet[Node](clause))
            }
          } else {
            if (posOc.isDefinedAt(v)) {
              posOc(v) += clause
            } else {
              posOc += (v -> MSet[Node](clause))
            }
          }
        }
      }
    })

    //start recursion
    res(posOc, negOc)
  }
  
  /**
  * Recursively resolves clauses, given two maps for positive/negative occurances of variables
  *
  * For TraceCheck chains, the following invariant holds:
  * At every point either
  * there exists a literal which occurs exactly once positively and once negatively
  * or there is only one clause remaining
  *
  * In the first case, this literal is used for resolving the respective clauses and updating the
  * occurange maps
  * In the other case, the one clause is returned
  * (either when no pivot is found or when the resolved clause is empty)
  */
  def res(posOc: MMap[Int,MSet[Node]], negOc: MMap[Int,MSet[Node]]): Node = {
    //println("IN RES")
    //println(posOc.mkString("posOc:\n", ", ", ""))
    //println(negOc.mkString("negOc:\n", ", ", ""))
    val nextPivot = posOc.find{
      // 1 occurrence pos and 1 neg
      case (l, n) => n.size == 1 && negOc.getOrElse(l, MSet.empty[Node]).size == 1
    }.map(opt => opt._1)
    
    // println(nextPivot)
    nextPivot match {
      //no more pivot means posOc and/or negOc can only contain 1 clause in the sets of occurances
      case None => {
        if (posOc.size > 0) {
          posOc.last._2.last
        } else {
          negOc.last._2.last
        }
      }
      case Some(piv) => {
        //println("piv: " + piv)
        val negClause = negOc(piv).last
        //println("neg: "+ negClause)
        val posClause = posOc(piv).last
        //println("pos: "+ posClause)
        internalCount += 1
        val newClause = Res.proofInit(negClause, posClause, piv << 1)
        //println("new: "+ newClause)

        newClause.resData.cl.labels.foreach{
          case (l, _) => {
            val v = Literals.toVar(l)

            if(Literals.isNeg(l)) {
              negOc(v) -= posClause
              negOc(v) -= negClause

              negOc(v) += newClause
            } else { 
              posOc(v) -= posClause
              posOc(v) -= negClause

              posOc(v) += newClause
            }
          }
        }
        val newPosOc = posOc - piv
        val newNegOc = negOc - piv

        if (newPosOc.isEmpty && newNegOc.isEmpty) {
          newClause
        } else {
          res(newPosOc, newNegOc)
        }
      }
    }
  }

  import scala.util.Random
  private val rnd = new Random(config.seed)
  private def computeRandomPartition(numLeaves: Int): Int = {
    rnd.nextInt(numLeaves >> 1) + (numLeaves >> 2) + 1
  }
  private def getLabel(id: Int, split: Int) = {
    def label = {
      if (id <= split) A
      else B
    }
    config.mode match {
      case Options.MinVars => label
      case Options.MinProof => Top
    }

  }
  
  def getNode(index: Int): Node = {
    if(clauses.isDefinedAt(index)) {
      clauses(index)
    } else {
      val newNode = clauseIds(index) match {
        case (lits, Nil) => {

          val label = (config.aPartitionEnd, config.bPartitionEnd) match {
            case (Some(a), Some(b)) => {
              getLabel(index, a)
            }
            case _ => {
              getLabel(index, computeRandomPartition(numInitClauses))
            }
          }

          initialCount += 1
          Init(lits, label)
        }
        case (_, ant) => {
          resolveClauses(ant)
        }
      }
      clauses(index) = newNode
      newNode
    }
  }

  /* Non-recursive version, but not working yet
  def computeProof(rootId: Int): Proof = {
    val Q = mutable.Queue[Int](rootId)
    val S = mutable.Stack.empty[(Int, List[Int])]
    val varIds = MSet.empty[Int]

    while(Q.nonEmpty) {
      val id = Q.dequeue
      val (lits, ants) = clauseIds(id)
      ants.foreach(antId => Q.enqueue(antId))

      if(ants.nonEmpty) {
        S.push(Pair(id, ants))
      } else if(!clauses.isDefinedAt(id)) {
        // Compute initial clauses if not yet done so
        varIds ++= lits.map(l => {
          Literals.toVar(l)  
        })

        val label = getLabel(id, computeRandomPartition(numInitClauses))
        clauses(id) = Init(lits, label)
      }
    }

    while(S.nonEmpty) {
      val (id, ants) = S.pop

      if(!clauses.isDefinedAt(id)) {
        
        val posOc = MMap.empty[Int, MSet[Node]]
        val negOc = MMap.empty[Int, MSet[Node]]

        //initialize the maps
        ants.foreach(antId => {
          val clause = clauses(antId)
          //println("id: "+ id)
          //println("clause: "+ clause)
          clause.resData.cl.labels.foreach{
            case (l, _) => {
              val v = Literals.toVar(l)

              if(Literals.isNeg(l)) {
                if (negOc.isDefinedAt(v)) {
                  negOc(v) += clause
                } else {
                  negOc += (v -> MSet[Node](clause))
                }
              } else {
                if (posOc.isDefinedAt(v)) {
                  posOc(v) += clause
                } else {
                  posOc += (v -> MSet[Node](clause))
                }
              }
            }
          }
        })

        clauses(id) = res(negOc, posOc)

      }
    }
    new Proof(clauses(rootId), varIds.size)
  }
  */
  
  def pos: Parser[Int] = """[1-9][0-9]*""".r ^^ { _.toInt }
  
  def neg: Parser[Int] = """-[1-9][0-9]*""".r ^^ { _.toInt }
  
  def literals: Parser[List[Int]] = ("*" | (lits <~ "0")) ^^ {
    case "*" => List.empty[Int]
    case l: List[Int] => l
  }
  
  def lits: Parser[List[Int]] = rep(lit)

  // Make internal variable representations start from 0
  // Makes it harder to debug, but keeps data structures smaller
  def lit: Parser[Int] = (pos | neg) ^^ { l => 
    val varId = varIds.getOrElseUpdate(math.abs(l), Id.fresh("var"))
    if(l < 0) {
      Literals.neg(varId << 1)
    } else {
      (varId << 1)
    }
  }
  
  def antecedents: Parser[List[Int]] = rep(pos) <~ "0"

}
