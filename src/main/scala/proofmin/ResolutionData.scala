package proofmin

import scala.collection._

class ResolutionData(
  val cl: Clause,
  val interpolantVars: InterpolantVars = immutable.Set.empty[Int],
  val itp: PropositionalFormula = Undef) {

  override def toString = {
    cl.labels.keySet.mkString("[", ", ", "]")
  }

  def toDot: String = {

    cl.labels.map {
      case (k, v) =>
        Literals.toPolarityRepresentation(k) + v.simple
    }.mkString("[", ", ", "]") +
      "" //interpolantVars.mkString(" [", ", ", "]")
  }
}

class MiscData(var relevantAncestors: Option[mutable.Set[Int]], var proofSize: Int)

object Res {

  def apply(neg: Node, pos: Node, piv: Int)(implicit config: Options.Config): ResolutionData = {
    assert(neg.resData.cl.labels.contains(Literals.neg(piv)) && pos.resData.cl.labels.contains(piv))

    val mergedLits = (neg.resData.cl.labels.keySet ++
      pos.resData.cl.labels.keySet) - piv - Literals.neg(piv)
    var mergedWithLabels = immutable.Map.empty[Int, Label]
    mergedLits.foreach { l =>
      mergedWithLabels += (l -> (neg.resData.cl.labels(l) merge pos.resData.cl.labels(l)))
    }
   
    if(config.mode == Options.MinVars) {
      val interpolantVars = if ((neg.resData.cl.labels(Literals.neg(piv)) merge
        pos.resData.cl.labels(piv)) == Top) {
        neg.resData.interpolantVars ++ pos.resData.interpolantVars + piv
      } else {
        neg.resData.interpolantVars ++ pos.resData.interpolantVars
      }

      val mergedLabel = neg.resData.cl.labels(Literals.neg(piv)) merge pos.resData.cl.labels(piv)
      val interpolant = mergedLabel match {
        case Top => (PropVar(piv) or pos.resData.itp) and (Not(PropVar(piv)) or neg.resData.itp)
        case A => pos.resData.itp or neg.resData.itp
        case B => pos.resData.itp and neg.resData.itp
        case _ => throw new Exception("invalid coloring")
      }

      new ResolutionData(Clause(mergedWithLabels), interpolantVars, interpolant)
    } else {
      new ResolutionData(Clause(mergedWithLabels))
    }
  }

  def proofInit(neg: Node, pos: Node, piv: Int)(implicit config: Options.Config): Inner = {

    val id = Id.fresh("node")

    val resData = Res(neg, pos, piv)

    val miscData = new MiscData(None, neg.miscData.proofSize + pos.miscData.proofSize + 1)

    val retVal = new Inner(id, resData, miscData, piv, Some(neg), Some(pos))
    
    neg.addChild(retVal, false)
    pos.addChild(retVal, true)

    retVal
  }
}

object Init {

  val initialVertices = mutable.Set.empty[Leaf]

  // lits already in posLitRepresentation (from parsing)
  def apply(lits: List[Int], coloring: Label)(implicit config: Options.Config): Leaf = {
    
    val id = Id.fresh("node")

    val clause = Clause(lits.map(l => (l -> coloring)).toMap)

    val resData = if(config.mode == Options.MinVars) {
      val itp = if(clause.labels.head._2 == A) False
        else if(clause.labels.head._2 == B) True
        else throw new Exception("unsupported labelling")
      new ResolutionData(clause, immutable.Set.empty[Int], itp)
    } else {
      new ResolutionData(clause)
    }

    val miscData = new MiscData(None, 1)

    val retVal = new Leaf(id, resData, miscData)
    initialVertices += retVal
    retVal 
  }
}

