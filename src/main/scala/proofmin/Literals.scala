package proofmin

object Literals {
  def toPosLitRepresentation(lit: Int): Int = {
    val retVal = (math.abs(lit) << 1) | (if (lit < 0) 1 else 0)
    assert(retVal > 0)
    retVal
  }
  
  def toPolarityRepresentation(lit: Int): Int = {
    val sign = (lit & 1) == 1
    if(sign) (lit >> 1) * -1 else (lit >> 1)
  }
  
  @inline def neg(lit: Int) = lit ^ 1
  @inline def toVar(lit: Int) = lit >> 1
  @inline def abs(lit: Int) = (lit >> 1) << 1 

  @inline def isNeg(lit: Int) = (lit & 1) == 1
}
