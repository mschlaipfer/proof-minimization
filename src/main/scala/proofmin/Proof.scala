package proofmin

import scala.collection._
import scala.collection.mutable.ArrayBuffer

class Proof(
  var root: Node,
  nbVars: Int,
  val initialVertices: mutable.Set[Leaf],
  var initialCount: Int,
  var internalCount: Int)
  (implicit config: Options.Config) {
  assert(root.resData.cl.labels.isEmpty)

  var watchLiterals: Array[regolic.sat.Vector[Clause]] = Array.fill(2 * (nbVars + 1))(new regolic.sat.Vector(20))

  // literal watching
  def registerNode(n: Node): Unit = {
    if(n != root && (config.recomputeAndSubsumption || config.timeLimit > 0)) {
      val cl = n.resData.cl
      val lit = cl.labels.keySet.head
      cl.setWatched(lit)
      this.watchLiterals(lit).append(cl)
      cl.setNode(n)
    }
  }

  // FIXME: assertion error
  // TODO remove from initialVertices if unregistered?
  def unregisterNode(n: Node): Unit = {
    if(n != root && (config.recomputeAndSubsumption || config.timeLimit > 0)) {
      val cl = n.resData.cl
      assert(cl.getWatched != -1)

      val ws = this.watchLiterals(cl.getWatched)
      ws.remove(cl)

      assert(this.watchLiterals.forall(list => !list.contains(cl)))

      cl.setWatched(-1)
    }
  }

  def initAncestorsAndRegisterNodes(): Unit = {
    val Q = mutable.Queue.empty[Inner]
    val V = mutable.Set.empty[Node]

    initialVertices.foreach(vx => {
      Q ++= vx.getChildren
      vx.miscData.relevantAncestors = Some(mutable.Set(vx.id))
      registerNode(vx)
    })
    V ++= initialVertices
    while(Q.nonEmpty) {
      val vx = Q.dequeue

      if(!V.contains(vx) && V.contains(vx.neg.get) && V.contains(vx.pos.get)) {
        V += vx
        Q ++= vx.getChildren

        registerNode(vx)

        val tmpRelevantAncestors = vx.neg.get.miscData.relevantAncestors.get ++
                                   vx.pos.get.miscData.relevantAncestors.get
        if(vx.getChildren.size > 1) tmpRelevantAncestors += vx.id
        vx.miscData.relevantAncestors = Some(tmpRelevantAncestors)
      }
    }
  }


  def getInterpolant: String = {
    
    val blif = new Blif(PropositionalFormula.inputs, this.root.resData.itp.rep)

    val formulaQ = mutable.Queue[PropositionalFormula](this.root.resData.itp)
    val formulaV = mutable.Set.empty[PropositionalFormula]
    while(formulaQ.nonEmpty) {
      val f = formulaQ.dequeue
      if(!formulaV.contains(f)) {
        formulaV += f

        if(f.toBlif != "")
          blif.addFormula(f.toBlif)

        f match {
          case And(f1, f2) => {
            formulaQ.enqueue(f1)
            formulaQ.enqueue(f2)
          }
          case Or(f1, f2) => {
            formulaQ.enqueue(f1)
            formulaQ.enqueue(f2)
          }
          case Not(f1) => {
            formulaQ.enqueue(f1)
          }
          case _ => ()
        }
      }
    }
    PropositionalFormula.reset

    blif.toString
  }

  // TODO maybe facilities for top-sorting/order later
}
