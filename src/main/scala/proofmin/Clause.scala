package proofmin

object Clause {
  def apply(elems: (Int, Label)*): Clause = new Clause(Map[Int, Label](elems.map {
    case (lit, label) => (Literals.toPosLitRepresentation(lit), label)
  }.toSeq: _*).withDefaultValue(Bottom))

  def apply(elems: Map[Int, Label]): Clause = new Clause(elems.withDefaultValue(Bottom))
}

class Clause private (
  val labels: Map[Int, Label]) {

  private var watched: Int = -1
  def setWatched(i: Int): Unit = watched = i
  def getWatched: Int = watched

  private var node: Node = null
  def setNode(n: Node): Unit = node = n
  def getNode: Node = node

  override def toString = labels.map {
    case (k, v) => (Literals.toPolarityRepresentation(k), v)
  }.mkString("[", ", ", "]")

  def merge(that: Clause): Clause = {
    val lits = this.labels.keySet union that.labels.keySet
    var retVal = Map.empty[Int, Label].withDefaultValue(Bottom)
    lits.foreach { l =>
      retVal += (l -> (this.labels(l) merge that.labels(l)))
    }
    new Clause(retVal)
  }

  def mergeOrdered(that: Clause): (Clause, Array[(Int, Label)]) = {
    val lits = this.labels.keySet union that.labels.keySet
    val order = new Array[(Int, Label)](lits.size)
    var retVal = Map.empty[Int, Label].withDefaultValue(Bottom)
    // this first
    var i = 0
    this.labels.keySet.foreach { l =>
      val mergedLabel = (this.labels(l) merge that.labels(l))
      retVal += (l -> mergedLabel)
      order(i) = Pair(l, mergedLabel)
      i += 1
    }
    val remainder = lits -- this.labels.keySet
    remainder.foreach { l =>
      val mergedLabel = (this.labels(l) merge that.labels(l))
      retVal += (l -> mergedLabel)
      order(i) = Pair(l, mergedLabel)
      i += 1
    }
    (new Clause(retVal), order)
  }

  def meet(that: Clause)(f: ((Set[Int], Set[Int]) => Set[Int])): Clause = {
    val lits = f(this.labels.keySet, that.labels.keySet)
    var retVal = Map.empty[Int, Label].withDefaultValue(Bottom)
    lits.foreach { l =>
      retVal += (l -> (this.labels(l) merge that.labels(l)))
    }
    new Clause(retVal)
  }

  def subsumes(that: Clause)(implicit config: Options.Config): Boolean = {
    this.labels.forall {
      case (lit, label) => that.labels.contains(lit) && (!config.suppressSubstitutions || label <= that.labels(lit))
    }
  }

  def distance(that: Clause): Int = {
    var retVal = 0
    this.labels.foreach {
      case (lit, label) => {
        if((that.labels.contains(lit) && (label merge that.labels(lit)) == Top)
            || (that.labels.contains(Literals.neg(lit)) && (label merge that.labels(Literals.neg(lit))) == Top)) {
          retVal += 1
        }
      }
    }
    retVal
  }

}
