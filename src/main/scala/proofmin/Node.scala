package proofmin

import scala.collection._

class BFSIterator(root: Node) extends Iterator[Node] {

  val S = mutable.Queue[Node](root)
  val V = mutable.BitSet()

  def hasNext = S.exists(n => !V.contains(n.id))

  def next = {
    if (S.nonEmpty) {
      var t = S.dequeue
      while (V.contains(t.id))
        t = S.dequeue
      V.add(t.id)

      t match {
        case Inner(Some(neg), Some(pos)) => {
          S.enqueue(neg)
          S.enqueue(pos)
        }
        case Inner(Some(neg), None) =>
          S.enqueue(neg)
        case Inner(None, Some(pos)) =>
          S.enqueue(pos)
        case _ => ()
      }
      t
    } else throw new NoSuchElementException
  }
}

class DFSIterator(root: Node) extends Iterator[Node] {

  val S = mutable.Stack[Node](root)
  val V = mutable.BitSet()

  def hasNext = S.exists(n => !V.contains(n.id))

  def next = {
    if (S.nonEmpty) {
      var t = S.pop
      // WTF? how is this terminating?
      while (V.contains(t.id))
        t = S.pop
      V.add(t.id)

      t match {
        case Inner(Some(neg), Some(pos)) => {
          S.push(neg)
          S.push(pos)
        }
        case Inner(Some(neg), None) =>
          S.push(neg)
        case Inner(None, Some(pos)) =>
          S.push(pos)
        case _ => ()
      }
      t
    } else throw new NoSuchElementException
  }
}


// Iterator for debugging purposes
class FollowLiteralIterator(val lit: Int, start: Node) extends Iterator[Node] {

  val S = mutable.Stack[Node](start)
  val V = mutable.BitSet()

  def hasNext = S.exists(n => !V.contains(n.id))

  var found = false

  def next = {
    if (S.nonEmpty) {
      var t = S.pop
      while (V.contains(t.id))
        t = S.pop
      V.add(t.id)
      if (t.resData.cl.labels.contains(lit))
        found = true

      t match {
        case Inner(Some(neg), Some(pos)) => {
          if (!found || neg.resData.cl.labels.contains(lit))
            S.push(neg)
          else if (!found || pos.resData.cl.labels.contains(lit))
            S.push(pos)
        }
        case Inner(Some(neg), None) =>
          if (!found || neg.resData.cl.labels.contains(lit))
            S.push(neg)
        case Inner(None, Some(pos)) =>
          if (!found || pos.resData.cl.labels.contains(lit))
            S.push(pos)
        case _ => ()
      }
      t
    } else throw new NoSuchElementException
  }
}

abstract class Node {

  var id: Int

  var resData: ResolutionData

  var miscData: MiscData

  protected val children = mutable.HashMap.empty[Inner, Boolean]

  def addChild(i: Inner, side: Boolean): Unit = {
    children += (i -> side)
    // TODO is relevantAncestors information updated correctly after adding a
    // child vertex?
  }

  /*
   *def addChildren(s: mutable.Set[(Inner, Boolean)]): Unit = {
   *  children ++= s
   *  s.foreach(_.domBy ++= this.domBy)
   *  if(children.size > 1)
   *    getChildren.foreach(_.domBy += this.id)
   *}
   */

  def removeChild(i: Inner): Unit = {
    children -= i
    freedomPaths -= i
  }

  def getChildrenWithSideInformation: mutable.HashMap[Inner,Boolean] = children

  def getChildren: Set[Inner] = children.keySet

  // Freedom/sigma stuff
  private var freedom = immutable.Map[Int, Label]().withDefaultValue(Bottom)

  var freedomPaths = immutable.Map.empty[Inner, (Boolean, Freedom)]

  def addPath(n: Inner, side: Boolean, f: Freedom) = {
    freedomPaths += (n -> Pair(side, f))
  }
  def setPath(n: Inner, side: Boolean, f: Freedom) = {
    // TODO add/setFreedomPath are essentially the same
    //assert(freedomPaths.contains(n))
    freedomPaths += (n -> Pair(side, f))
  }

  def setFreedom(f: Freedom) = {
    freedom = f
  }
  def getFreedom: Freedom = {
    freedom
  }


  override def toString = {
    val freedomStr = ""
    //      this match {
    //      case Leaf(_) => freedom.map { case (k, v) => k + v.simple }.mkString("[", ", ", "]")
    //      case Inner(_, _, _) => ""
    //    }
    val labelStr = "label=\"" + this.resData.toDot + freedomStr + "\""

    val pivString: String = if (this.isInstanceOf[Inner])
      " " + Literals.toPolarityRepresentation(this.asInstanceOf[Inner].piv)
    else ""

    val ancString: String = if (this.isInstanceOf[Inner]) {
      " -" + (if(this.asInstanceOf[Inner].neg == None) "<NONE>" else "") +
      " +" + (if(this.asInstanceOf[Inner].pos == None) "<NONE>" else "")
    }
    else ""

    this.id + s"$pivString [$labelStr]$ancString;"
  }

  override def hashCode = id
  override def equals(other: Any) = other match {
    case that: Node =>
      (that canEqual this) &&
        (this.id == that.id)
    case _ =>
      false
  }

  def canEqual(other: Any) = other.isInstanceOf[Node]

}

case class Leaf (
  var id: Int,
  var resData: ResolutionData,
  var miscData: MiscData) extends Node {

  override def hashCode = super.hashCode

  override def equals(other: Any) = other match {
    case that: Node =>
      (that canEqual this) &&
        super.equals(that)
    case _ =>
      false
  }

  override def canEqual(other: Any) =
    other.isInstanceOf[Leaf]
}

object Inner {
  def apply(id: Int, resData: ResolutionData, miscData: MiscData, piv: Int, neg: Option[Node], pos: Option[Node]) = 
    new Inner(id, resData, miscData, piv, neg, pos)

  def unapply(i: Inner): Option[(Option[Node], Option[Node])] = Some(Pair(i.neg, i.pos))
}

class Inner (
  var id: Int,
  var resData: ResolutionData,
  var miscData: MiscData,
  var piv: Int,
  var neg: Option[Node],
  var pos: Option[Node]) extends Node {

  override def hashCode = super.hashCode

  override def equals(other: Any) = other match {
    case that: Node =>
      (that canEqual this) &&
        super.equals(that)
    case _ =>
      false
  }

  override def canEqual(other: Any) =
    other.isInstanceOf[Inner]

}
