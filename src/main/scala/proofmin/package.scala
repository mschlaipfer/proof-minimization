package object proofmin {

//  type Clause = scala.collection.immutable.Map[Int, Label]
  type Freedom = scala.collection.immutable.Map[Int, Label]
  type InterpolantVars = scala.collection.immutable.Set[Int]
  
  def outln(str: String)(implicit config: Options.Config): Unit = {
    if(config.verbose)
      println(str)
  }
  
  def outBlock(bl: => Unit)(implicit config: Options.Config): Unit = {
    if(config.verbose)
      bl
  }
}
