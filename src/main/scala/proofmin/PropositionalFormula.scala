package proofmin

object PropositionalFormula {
  val inputs = collection.mutable.Set.empty[String]

  def freshRep: String = {
    "rep_"+ Id.fresh("prop")
  }

  // XXX this is very dangerous, as it can be easily forgotten
  def reset: Unit = {
    inputs.clear
    Id.reset("prop")
  }
}

trait PropositionalFormula {

  override def hashCode = rep.hashCode

  val rep: String

  def or(that: PropositionalFormula): PropositionalFormula = {
    Or(this, that)
  }
  def and(that: PropositionalFormula): PropositionalFormula = {
    And(this, that)
  }
  def not: PropositionalFormula = {
    Not(this)
  }

  def toBlif: String
}

object PropVar {
  def apply(v: Int): PropositionalFormula = {
    val input = "in_"+ v.toString
    PropositionalFormula.inputs += input
    new PropVar(input)
  }
}
class PropVar(val rep: String) extends PropositionalFormula {
  def toBlif = ""
}

case object False extends PropositionalFormula {
  val rep = ""
  
  def toBlif = ""
}
case object True extends PropositionalFormula {
  val rep = ""

  def toBlif = ""
}

object Or {
  def apply(f1: PropositionalFormula, f2: PropositionalFormula): PropositionalFormula = {
    f1 match {
      case True => True
      case False => f2
      case _ => {
        f2 match {
          case True => True
          case False => f1
          case _ => new Or(f1, f2)
        }
      }
    }
  }

  def unapply(or: Or): Option[(PropositionalFormula, PropositionalFormula)] = Some((or.f1, or.f2))
}

class Or(val f1: PropositionalFormula, val f2: PropositionalFormula) extends PropositionalFormula {

  val rep = {
    PropositionalFormula.freshRep
  }

  def toBlif: String = {
    s".names ${f1.rep} ${f2.rep} ${this.rep}\n" +
    "11 1\n" +
    "01 1\n" +
    "10 1"
  }

  override def toString: String = {
    "-----------------------------------------------\n"+
    rep + "\n"+
    toBlif +"\n"+
    "-----------------------------------------------\n"
  }
}

object And {
  def apply(f1: PropositionalFormula, f2: PropositionalFormula): PropositionalFormula = {
    f1 match {
      case True => f2
      case False => False
      case _ => {
        f2 match {
          case True => f1
          case False => False
          case _ => new And(f1, f2)
        }
      }
    }
  }

  def unapply(and: And): Option[(PropositionalFormula, PropositionalFormula)] = Some((and.f1, and.f2))
}

class And(val f1: PropositionalFormula, val f2: PropositionalFormula) extends PropositionalFormula {

  val rep = {
    PropositionalFormula.freshRep
  }

  def toBlif: String = {
    s".names ${f1.rep} ${f2.rep} ${this.rep}\n" +
    "11 1"
  }

  override def toString: String = {
    "-----------------------------------------------\n"+
    rep + "\n"+
    toBlif +"\n"+
    "-----------------------------------------------\n"
  }
}

object Not {
  def apply(f1: PropositionalFormula): PropositionalFormula = {
    f1 match {
      case True => False
      case False => True
      case _ => new Not(f1)
    }
  }

  def unapply(not: Not): Option[PropositionalFormula] = Some(not.f1)
}

class Not(val f1: PropositionalFormula) extends PropositionalFormula {

  val rep = {
    PropositionalFormula.freshRep
  }

  def toBlif: String = {
    s".names ${f1.rep} ${this.rep}\n" +
    "0 1"
  }

  override def toString: String = {
    "-----------------------------------------------\n"+
    rep + "\n"+
    toBlif +"\n"+
    "-----------------------------------------------\n"
  }
}

case object Undef extends PropositionalFormula {
  val rep = "" 

  def toBlif = ""
}

