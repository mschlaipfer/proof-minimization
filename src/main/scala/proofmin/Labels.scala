package proofmin

abstract class Label {
  def merge(that: Label): Label = {
    this match {
      case A => that match {
        case A | Bottom => A
        case _ => Top
      }
      case B => that match {
        case B | Bottom => B
        case _ => Top
      }
      case Top => Top
      case Bottom => that
    }
  }
  
  def intersect(that: Label): Label = {
    this match {
      case A => that match {
        case A | Top => A
        case _ => Bottom
      }
      case B => that match {
        case B | Top => B
        case _ => Bottom
      }
      case Top => that
      case Bottom => Bottom
    }
  }

  def <=(that: Label): Boolean = {
    this match {
      case Top => that match {
        case Top => true
        case _ => false
      }
      case A => that match {
        case A | Top => true
        case _ => false
      }
      case B => that match {
        case B | Top => true
        case _ => false
      }
      case _ => true
    }
  }
  
  def >(that: Label): Boolean = {
    !(<=(that))
  }

  val simple: String
  override def toString = simple
}
case object Top extends Label {
  val simple = "ab"
}
case object A extends Label {
  val simple = "a"
}
case object B extends Label {
  val simple = "b"
}
case object Bottom extends Label {
  val simple = "bot"
}
