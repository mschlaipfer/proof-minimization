#!/bin/bash

directory=$1/

cnfdir=$directory/cnfs/
proofdir=$directory/proofs_minisat/

if [[ -d $cnfdir ]]; then
  rm -rf $cnfdir
fi

mkdir $cnfdir

if [[ -d $proofdir ]]; then
  rm -rf $proofdir
fi

mkdir $proofdir

while read FILE; do
  # do something with LINE
  bn=$(basename -s .proof $FILE)
  path=$(find $directory -name $bn)
  echo "Running "$path
  #timeout 1m ~/tools/aiger-1.9.4/aigunroll 10 $path | ~/tools/aiger-1.9.4/aigtocnf > $cnfdir$bn.cnf
  cp $path $cnfdir$bn.cnf
  timeout 5m ./MiniSat-p_v1.14/minisat $cnfdir$bn.cnf -c > $proofdir$bn.proof
  rc=$?
  if [[ $rc != 20 ]]; then
    echo "TIMEOUT"
    rm $proofdir$bn.proof
    continue
  fi
  echo "Done "$bn
done < $1/feasible.txt

