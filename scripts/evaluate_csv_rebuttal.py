import os
import sys
if len(sys.argv) > 1 and os.path.isdir(sys.argv[1]):
  os.chdir(sys.argv[1])
else:
  print "Usage: python evaluate_csv.py <dir>"
  sys.exit()

run_count = 6

goals = ['vars']
modes = ['normal', 'no-suppress']

#triples now a pair
def gen_run_names(goal):
  triples = []
  if goal == 'proof':
    triple = []
    for m in modes:
      triple.append("min-"+goal+"-"+m)
    triples.append(triple)
  elif goal == 'vars':
    runs = range(0, run_count)
    for i in runs:
      triple = []
      for m in modes:
        triple.append("min-"+goal+"-"+m+str(i))
      triples.append(triple)
  else:
    raise Exception()
  return triples

max_proof_size = 0
def compute_avg_run(data_arrays, valid_indices):
  global max_proof_size
  avg_run = []
  for data in data_arrays:
    sums = {'ands_unmin': 0.0, 'levs_unmin': 0.0, 'ands_min': 0.0, 'levs_min': 0.0, 'vars': 0.0, 'size': 0.0, 'initial': 0.0, 'internal': 0.0, 'time': 0.0}
    for i in valid_indices:
      if data[i][0] > max_proof_size:
        max_proof_size = data[i][0]
      sums['ands_unmin'] += data[i][1]
      sums['levs_unmin'] += data[i][2]
      sums['ands_min'] += data[i][3]
      sums['levs_min'] += data[i][4]
      sums['vars'] += data[i][5]
      sums['size'] += data[i][6]
      sums['initial'] += data[i][7]
      sums['internal'] += data[i][8]
      sums['time'] += data[i][9]
    avgs = {k: v / len(valid_indices) for k, v in sums.items()}
    avg_run.append(avgs)
  return avg_run

def is_split_valid(split):
  return not ('t/o' in split or 'm/o' in split or 'NaN' in split or int(split[4]) < 100)

def compute_valid_indices(goals):
  import sets
  valid_indices = sets.Set()
  offset = 2
  init = True
  for goal in goals:
    triples = gen_run_names(goal)
    for triple in triples:
      for run in triple:
        with open(run+".csv", "r") as f:
          for (i,line) in enumerate(f):
            if i > 1:
              split = line.split(';')
              if init and is_split_valid(split):
                valid_indices.add(i - offset)
              elif not init and not is_split_valid(split) and (i-offset) in valid_indices:
                valid_indices.remove(i - offset)
          f.close()
          # prevent readding of removed stuff
          init = False
  return valid_indices

def gen_data():
  result = []
  valid_indices = compute_valid_indices(goals)
  for goal in goals:
    triples = gen_run_names(goal)
    goal_results = []
    
    for triple in triples:
      data_arrays = []

      for run in triple:
        with open(run+"_corrected.csv", "w+") as corrected:
          with open(run+".csv", "r") as f:
            for (i,line) in enumerate(f):
              split = line.split(';')
              if 't/o' in split or 'm/o' in split:
                corrected.write(line[:-1] + ";;;;;;;;;;;;;;;;;;;;;;;;;;" + "\n")
              else:
                corrected.write(line[:-2] + "\n")
            f.close()
          corrected.close()

        import numpy as np
        data = np.genfromtxt(run+'_corrected.csv', delimiter=';', skip_header=1,
                       names=True, dtype=None, usecols=(4,6,7,13,14,18,19,20,21,27))

        data_arrays.append(data)
        os.remove(run+"_corrected.csv")

      avg_run = compute_avg_run(data_arrays, valid_indices)
      goal_results.append(avg_run)

    sums = []
    for i in range(len(modes)):
      sums.append({'ands_unmin': 0.0, 'levs_unmin': 0.0, 'ands_min': 0.0, 'levs_min': 0.0, 'vars': 0.0, 'size': 0.0, 'initial': 0.0, 'internal': 0.0, 'time': 0.0})
    for goal_result in goal_results:
      for i in range(len(modes)):
        sums[i]['ands_unmin'] += goal_result[i]['ands_unmin']
        sums[i]['levs_unmin'] += goal_result[i]['levs_unmin']
        sums[i]['ands_min'] += goal_result[i]['ands_min']
        sums[i]['levs_min'] += goal_result[i]['levs_min']
        sums[i]['vars'] += goal_result[i]['vars']
        sums[i]['size'] += goal_result[i]['size']
        sums[i]['initial'] += goal_result[i]['initial']
        sums[i]['internal'] += goal_result[i]['internal']
        sums[i]['time'] += goal_result[i]['time']
    avgs = []
    for i in range(len(modes)):
      avgs.append({k: v / len(goal_results) for k, v in sums[i].items()})
    result.append(avgs)

  print "# valid benchmarks:", len(valid_indices)
  print "max proof size:", max_proof_size

  return result
        
def to_latex(results):
  for r in zip(modes, results):
    print r

print "Displayed are improvements in % to original proof and the average runtime"
for (g, item) in zip(goals, gen_data()):
  print "minimizing", g
  to_latex(item)
