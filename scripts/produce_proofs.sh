#!/bin/bash

directory=$1/

proofdir=$directory/proofs/

if [[ -d $proofdir ]]; then
  rm -rf $proofdir
fi

mkdir $proofdir

for FILE in $( find $directory -name "*.aig" | sort -V )
do
  echo "Running "$FILE
  bn=$(basename $FILE .aig)

  tmp_unrolled=/tmp/unrolled.aig
  tmp_cnf=/tmp/cnf
  tmp_trace=$proofdir$bn
  k=1
  secs=30   # Set interval (duration) in seconds.
  SECONDS=0   # Reset $SECONDS; counting of seconds will (re)start from 0.
  while (( SECONDS < secs )) && [[ ! $(find $tmp_trace"_f"* -type f -size -10000k 2>/dev/null) || ! $(find $tmp_trace"_f"* -type f -size +10000k 2>/dev/null) ]] ; do    # Loop until interval has elapsed.
    ~/tools/aiger-1.9.4/aigunroll $k $FILE > $tmp_unrolled
    ~/tools/aiger-1.9.4/aigtocnf $tmp_unrolled > $tmp_cnf
    timeout 1m ~/tools/picosat-959/picosat -t $tmp_trace"_f"$((k % 2)) $tmp_cnf > /dev/null
    ((k++))
  done
  $(find $tmp_trace"_f"* -type f -size -10000k 2>/dev/null -exec mv {} $tmp_trace"_"$((k-1))".trace" \; | head -n 1)
  #$(find $tmp_trace"_f"* -type f -size -10000k 2>/dev/null -exec mv {} $tmp_trace"_"$((k-1))".trace" \; | head -n 1)
  $(find $tmp_trace"_f"* -type f 2>/dev/null -exec rm -f {} \;)
  echo "Done "$FILE
done
