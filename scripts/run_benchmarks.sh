#!/bin/bash

directory=$1/

csv=$2

fwdargs=${@:3}

#echo "$(date);$(git rev-parse HEAD)" >> $csv
echo "$(date);$fwdargs" >> $csv
echo "name;"$(java -jar ./proof-minimization_2.10-0.1-one-jar.jar --print-header .) >> $csv

for FILE in $( find $directory -name "*.proof" | sort -V )
do
  echo "Running "$FILE
  bn=$(basename $FILE)
  result=$(timeout 15m java -Xmx16g -jar ./proof-minimization_2.10-0.1-one-jar.jar $fwdargs $FILE)
  rc=$?
  if [[ $rc -eq 124 ]]; then
    echo "$bn;t/o;" >> $csv
    echo "Timeout "$FILE
  elif [[ $rc != 0 ]]; then
    echo "$bn;m/o;" >> $csv
    echo "Memout "$FILE
  else
    echo "$bn;$result" >> $csv
    echo "Done "$FILE
  fi
done
