#!/bin/bash
mode=$1
benchmarks=$2
run=$3
randomSeed=$RANDOM
if [ $mode == "r3" ]
then 
  echo "Running Ashutosh in proofs/"
  ./run_benchmarks.sh "$mode"benchmarks/$benchmarks "$mode"benchmarks/$benchmarks/data/min-vars-ashutosh$run.csv mode --min-vars -a --set-seed $randomSeed 2>> "$mode"benchmarks/debug/$benchmarks-min-vars-ashutosh$run.out
  echo "Running -t 0 in proofs/"
  ./run_benchmarks.sh "$mode"benchmarks/$benchmarks "$mode"benchmarks/$benchmarks/data/min-vars-t0$run.csv mode --min-vars -t 0 --set-seed $randomSeed 2>> "$mode"benchmarks/debug/$benchmarks-min-vars-t0$run.out
  echo "Running -t 15 in proofs/"
  ./run_benchmarks.sh "$mode"benchmarks/$benchmarks "$mode"benchmarks/$benchmarks/data/min-vars-t10$run.csv mode --min-vars -t 10 --set-seed $randomSeed 2>> "$mode"benchmarks/debug/$benchmarks-min-vars-t10$run.out
elif [ $mode == "minisat" ]
then
  echo "Running Ashutosh in proofs/"
  ./run_benchmarks.sh "$mode"benchmarks/$benchmarks "$mode"benchmarks/$benchmarks/data/min-vars-ashutosh$run.csv mode --min-vars -c -a --set-seed $randomSeed 2>> "$mode"benchmarks/debug/$benchmarks-min-vars-ashutosh$run.out
  echo "Running -t 0 in proofs/"
  ./run_benchmarks.sh "$mode"benchmarks/$benchmarks "$mode"benchmarks/$benchmarks/data/min-vars-t0$run.csv mode --min-vars -c -t 0 --set-seed $randomSeed 2>> "$mode"benchmarks/debug/$benchmarks-min-vars-t0$run.out
  echo "Running -t 15 in proofs/"
  ./run_benchmarks.sh "$mode"benchmarks/$benchmarks "$mode"benchmarks/$benchmarks/data/min-vars-t10$run.csv mode --min-vars -c -t 10 --set-seed $randomSeed 2>> "$mode"benchmarks/debug/$benchmarks-min-vars-t10$run.out
else
  echo "Usage r3|minisat hwmcc|mus number"
fi
