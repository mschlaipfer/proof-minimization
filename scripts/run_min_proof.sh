#!/bin/bash
mode=$1
benchmarks=$2
if [ $mode == "r3" ]
then 
  echo "Running Ashutosh in proofs/"
  ./run_benchmarks.sh "$mode"benchmarks/$benchmarks "$mode"benchmarks/$benchmarks/data/min-proof-ashutosh.csv mode --min-proof -a 2>> "$mode"benchmarks/debug/$benchmarks-min-proof-ashutosh.out
  echo "Running -t 0 in proofs/"
  ./run_benchmarks.sh "$mode"benchmarks/$benchmarks "$mode"benchmarks/$benchmarks/data/min-proof-t0.csv mode --min-proof -t 0 2>> "$mode"benchmarks/debug/$benchmarks-min-proof-t0.out
  echo "Running -t 15 in proofs/"
  ./run_benchmarks.sh "$mode"benchmarks/$benchmarks "$mode"benchmarks/$benchmarks/data/min-proof-t10.csv mode --min-proof -t 10 2>> "$mode"benchmarks/debug/$benchmarks-min-proof-t10.out
elif [ $mode == "minisat" ]
then
  echo "Running Ashutosh in proofs/"
  ./run_benchmarks.sh "$mode"benchmarks/$benchmarks "$mode"benchmarks/$benchmarks/data/min-proof-ashutosh.csv mode --min-proof -c -a 2>> "$mode"benchmarks/debug/$benchmarks-min-proof-ashutosh.out
  echo "Running -t 0 in proofs/"
  ./run_benchmarks.sh "$mode"benchmarks/$benchmarks "$mode"benchmarks/$benchmarks/data/min-proof-t0.csv mode --min-proof -c -t 0 2>> "$mode"benchmarks/debug/$benchmarks-min-proof-t0.out
  echo "Running -t 15 in proofs/"
  ./run_benchmarks.sh "$mode"benchmarks/$benchmarks "$mode"benchmarks/$benchmarks/data/min-proof-t10.csv mode --min-proof -c -t 10 2>> "$mode"benchmarks/debug/$benchmarks-min-proof-t10.out
else
  echo "Usage [r3|minisat] [hwmcc|mus]"
fi
