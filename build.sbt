seq(com.github.retronym.SbtOneJar.oneJarSettings: _*)

name := "proof minimization"

version := "0.1"

scalaVersion := "2.10.3"

libraryDependencies ++= Seq("commons-lang" % "commons-lang" % "2.6",
                            "org.scalatest" % "scalatest_2.10" % "1.9.1" % "test",
                            "com.github.scopt" %% "scopt" % "3.2.0")

resolvers += Resolver.sonatypeRepo("public")

scalacOptions += "-feature"

mainClass in oneJar := Some("proofmin.Main")
